var mongoose = require("mongoose");
var Schema = require("mongoose").Schema;

var SubscriptionSchema = Schema({
    plan_id: { type: String },
    user_id : { type: Schema.Types.ObjectId, ref : 'users'},
    subscription_price: { type: Number },
    listed_date : {type: Date, default : Date.now},
    listed_end_date : {type: Date},
    status: { type: String, enum: ["Enable", "Disable"], default: "Enable" }

});
SubscriptionSchema.methods.toJSON = function() {
    var obj = this.toObject();
    delete obj.__v;
    return obj;
}
module.exports = mongoose.model('Subscription', SubscriptionSchema);
