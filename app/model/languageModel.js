var mongoose = require('mongoose');
var Schema = mongoose.Schema;

languageSchema = new Schema({
   name: {type: String},
   code : {type: String}
   });

var languages = mongoose.model('languages', languageSchema);
module.exports = languages;
