var mongoose = require("mongoose");
var Schema = require("mongoose").Schema;

var CounterSchema = Schema({
	id_name: {
		type: String
	},
	seq: {
		type: Number,
		default: 0
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});
module.exports = mongoose.model('Counter', CounterSchema);