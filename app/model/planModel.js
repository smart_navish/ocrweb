var mongoose = require('mongoose');
var Schema = mongoose.Schema;

planSchema = new Schema({
   name: {type: String},
   price : {type: Number},
   days : {type : Number}
});

var plans = mongoose.model('plans', planSchema);
module.exports = plans;
