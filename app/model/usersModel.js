var mongoose = require('mongoose');
var Schema = mongoose.Schema;

usersSchema = new Schema({
   email: {type: String, default : null},
   fb_id : {type : String, default : null},
   password : {type : String},
   fullName : {type : String},
   phoneNumber : {type : String},
   language : { type: Schema.Types.ObjectId, ref: 'languages'},
   confirmationToken : {type : Number},
   forgotToken : {type : Number},
   subscription : { type: Schema.Types.ObjectId, ref: 'Subscription'},
   status : {type : Boolean, default: true},
   verified : {type : Boolean, default: false},
   isDeleted : {type : Boolean, default: false},
   updatedOn : {type : Date, default: Date.now}
   }
);

var users = mongoose.model('users', usersSchema);
module.exports = users;
