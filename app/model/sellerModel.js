var mongoose = require("mongoose");
var Schema = require("mongoose").Schema;

var sellerloginSchema = Schema({
    username:{ type:String },
    email:{ type:String },
    password:{ type:String },
    first_name:{ type:String },
    last_name:{ type:String },
    role:{ type:String },
    isDeleted:{ type:Boolean, default: false },
    status:{ type:String },
    randomstring:{ type:String},
    AddressLine1 : {type : String},
  	AddressLine2 : {type : String},
  	AddressLine3 : {type : String},
  	Country: {type : String},
  	State: {type : String},
  	City: {type : String},
  	PIN: {type : String},
  	ContactNo : {type : String},
  	IsVerified : {type : Boolean, default : false},
  	SecondaryEmail : {type : String},
  	ProfilePic : {type : String},
  	CreatedAt : {type : Date, default : Date.now },
  	LastLogin : {type : String, default : "0"},
  	//SubscriptionDetails : [subscriptionSchema],
  	SubscriptionPlanName : {type : String, enum : ['NoPlan','Basic','Intermediate','Advanced'], default : 'NoPlan'},
  	SubscriptionActivatedOn : {type : Date},
  	SubscriptionExpiredOn : {type : Date},
  	PaymentValidTill : {type : Date},
  	IntialPayment : {type : Boolean, default : false},
  	SubscriptionActive : {type : Boolean, default : false},
  	SubscriptionProfileId : {type : String},
  	ActivatedOn : {type : Date, default : Date.now},
  	IsChecked : {type : Boolean, default : true},
  	LastActiveTime : {type : Date, default : Date.now},
  	IsLoggedIn  : {type : Number, default : 0},
  	IpAddress : {type : String},
    targetMargin : {type:Number}
})

sellerloginSchema.methods.toJSON = function(){
	var obj = this.toObject();
	delete obj.__v;
	return obj;
}

module.exports = mongoose.model('Seller', sellerloginSchema);
