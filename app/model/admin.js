var mongoose = require("mongoose");
var Schema = require("mongoose").Schema;

var AdminSchema = Schema({
	username: {
		type: String
	},
	email: {
		type: String
	},
	password: {
		type: String
	},
	first_name: {
		type: String
	},
	last_name: {
		type: String
	},
	isDeleted: {
		type: Boolean,
		default: false
	},
	designation: {
		type : String,
		default : "Admin"
	},
	createdOn: {
		type : Date,
		default : new Date()
	},
	randomstring: {
		type: String
	}
});

AdminSchema.methods.toJSON = function() {
	var obj = this.toObject()
	delete obj.__v;
	return obj;
}

module.exports = mongoose.model('Admin', AdminSchema);
