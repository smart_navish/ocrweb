var mongoose = require("mongoose");
var Schema = require("mongoose").Schema;

var adminloginSchema = Schema({
    username:{
    	type:String
    },
    email:{
    	type:String
    },
    password:{
    	type:String
    }
})

adminloginSchema.methods.toJSON = function(){
	var obj = this.toObject();
	delete obj.__v;
	return obj;
}

module.exports = mongoose.model('login', adminloginSchema);