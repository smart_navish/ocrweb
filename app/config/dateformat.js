module.exports = function (dateData, isUtc = false) {
  var date = new Date(dateData);
  var utcDate = new Date(date.toUTCString());
  if(isUtc == true){
      utcDate.setHours(utcDate.getHours() + 6);
      console.log('in 6 date------------------------------------');
  }else {
      utcDate.setHours(utcDate.getHours() - 7);
      console.log('in 7 date------------------------------------');
  }
  var usDate = new Date(utcDate);
  return usDate;
}
