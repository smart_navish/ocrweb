// this module uses local-strategy and admin is login using this module
module.exports = function(app, express, passport) {

	// var tokenService = require('./../app/token/tokenAuth.js');
	// var userTokenObj = require('./../app/models/user/userTokens.js');
	var sellerLoginDB = require('./../model/sellerModel.js');
	var adminDB = require("./../model/admin.js");
	var LocalStrategy = require('passport-local').Strategy;
	var jsmd5 = require('js-md5');

	passport.authenticate('LocalStrategy', {
		session: true
	})
	passport.use('local', new LocalStrategy(
		function(username, password, done) {
			console.log("username>>>", username);
			console.log("password>>>", password);
			var newPassword = jsmd5(password);
			console.log("newPassword",newPassword);
			sellerLoginDB.findOne({
				email: username,
				password: newPassword,
				isDeleted:false,
				status:'active'
			}, function(err, adminuser) {
				if (err) {

					return done(err);
				} else {
					console.log("adminuser", adminuser);

					// if (adminuser.length== 0) {
					// 	return done(null, false);
					// }
					if (adminuser == null) {
						console.log("here");
						return done(null, false);
					} else {
						return done(null, {
							user: adminuser
						})
					}
				}
				// if (adminuser.emailVerfication) {
				// 	return done(null, {message:"Please verify your account first"});
				// }

				// //returning specific data
				// //generate a token here and return
				// var authToken = tokenService.issueToken({
				// 	sid: adminuser
				// });
				// // save token to db  ;
				// var tokenObj = new userTokenObj({
				// 	"admin": adminuser._id,
				// 	"token": authToken
				// });

				// tokenObj.save(function(e, s) {});
				// console.log("Type is " , adminuser.type);
				//return permission from here
				// return done(null, {
				// 	id: adminuser._id,
				// 	companyId: adminuser.companyId,
				// 	firstname: adminuser.firstName,
				// 	lastname: adminuser.lastName,
				// 	userRole: adminuser.userRole,
				// 	userEmail: adminuser.userEmail,
				// 	userMobile: adminuser.userMobile,
				// 	siteIds :adminuser.site_Id,
				// 	token: authToken
				// });
			});
		}
	));

	
	passport.use('localadmin', new LocalStrategy(
		function(username, password, done) {
			var newPassword = jsmd5(password);
			adminDB.findOne({
				email: username,
				password: newPassword,
				isDeleted:false
			}, function(err, adminuser) {
				if (err) {

					return done(err);
				} else {
					
					if (adminuser == null) {
		
						return done(null, false);
					} else {
						
						return done(null, {
							user: adminuser
						})
					}
				}

			});
		}
	));
	/*passport.serializeUser(adminLoginObj.serializeUser);
	passport.deserializeUser(adminLoginObj.deserializeUser);*/

	passport.serializeUser(function(adminLoginObj, done) {
		done(null, adminLoginObj);
	});

	passport.deserializeUser(function(adminLoginObj, done) {
		done(null, adminLoginObj);
	});
}
