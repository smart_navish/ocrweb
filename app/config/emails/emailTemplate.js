var nodemailer = require('nodemailer');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
// create reusable transporter object using the default SMTP transport
// var transporter = nodemailer.createTransport('smtps://ef1e56e4d7aa7f40a72ac817648c7b1d:1e3c41b33c68fb44f6e841f3b63867ce@in-v3.mailjet.com');
var setSMTP = {
 service: 'gmail',
 auth: {
        user: 'smartdata.nav@gmail.com',
        pass: 'rssb@123#'
    }
}
var transporter = nodemailer.createTransport(setSMTP);
module.exports = function(type, sendTo, bodyInfo, callback) {
    console.log("In email", bodyInfo);

    if (sendTo) {
        var emails = sendTo.split(",");
        var sendTo = emails[0].replace(/\s+/g, '');
        var ccemail = "";
        if (emails.length > 1) {
            for (var i = 1; i < emails.length; i++) {
                ccemail = (i == 1 ? (emails[i].replace(/\s+/g, '')) : (ccemail + ', ' + emails[i].replace(/\s+/g, '')));
                console.log("cc", ccemail);
            }
        }
    }
    var mailOptions = {
        from: '"OCR" <smartdata.nav@gmail.com>', // sender address
    };
    var htmlFile;
    switch(type)
    {
        case 'RegisterCode':
        htmlFile = fs.readFileSync(path.join(__dirname + '/template/registerUser.html'), 'utf-8');
        htmlFile = htmlFile.replace('$confirmationToken', bodyInfo.confirmationToken).replace('$logo', bodyInfo.logo);
        mailOptions.to = sendTo;
        mailOptions.subject = 'Confirmation token';
        mailOptions.html = htmlFile;
        sendMail(mailOptions, function(emailerr, emailsuccess) {
            if (emailerr) {
                return callback(emailerr, null);
            } else {
                return callback(null, emailsuccess)
            }
        });
        break;
        case 'ForgotPassword':
        htmlFile = fs.readFileSync(path.join(__dirname + '/template/forgotPassword.html'), 'utf-8');
        htmlFile = htmlFile.replace('$ForgotOTP', bodyInfo.ForgotOTP).replace('$logo', bodyInfo.logo);
        mailOptions.to = sendTo;
        mailOptions.subject = 'Forgot Password';
        mailOptions.html = htmlFile;
        //console.log('ddddddddddd', htmlFile);
        sendMail(mailOptions, function(emailerr, emailsuccess) {
            if (emailerr) {
                return callback(emailerr, null);
            } else {
                return callback(null, emailsuccess)
            }
        });
        break;
    }

    function sendMail(mailOptions, cb) {

        var a = transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
              //  return cb(error, null);
            } else {
                console.log('Message sent: ' + info.response);
                //return cb(null, info);
            }
        });
        return cb(null, null);
        //setTimeout(function(){ return a.abort(); cb(null, null) }, 1500);

    }
};
