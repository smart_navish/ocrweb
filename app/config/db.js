/*
 * The file will take care of the database connectivity
 */
module.exports = function(config) {
    var mongoose = require('mongoose');
    mongoose.connect(config.database);
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
}
