var sellerLoginDB = require("./../model/sellerModel.js");
var emails = require("./../config/emails/emailTemplate.js");
var jsmd5 = require('js-md5');

//Encryption And Decryption
var crypto = require('crypto'),
	algorithm = 'aes-256-ctr',
	password = 'd6F3Efeq';
encrypt = function(text) {
	var cipher = crypto.createCipher(algorithm, password)
	var crypted = cipher.update(text, 'utf8', 'hex')
	crypted += cipher.final('hex');
	return crypted;
}
decrypt = function(text) {
	var decipher = crypto.createDecipher(algorithm, password)
	var dec = decipher.update(text, 'hex', 'utf8')
	dec += decipher.final('utf8');
	return dec;
}

makeid = function() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < 15; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	}
	//END


//Login Function
exports.login = function(req, res) {
	console.log("params**********************",req.params);
		try {

			req.session.user = res.req.user;
			if (res.req.user) {
				var outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': 'login successfully',
					'data': res.req.user

				}
				return res.send(outputJSON);
			}

		} catch (e) {

		}
	}
	//END


exports.getAccessToken = function(req,res){
	console.log("req.body",req.body);
}

//Function to Check Session
exports.checkSession = function(req, res) {
		try {


			if (req.session.user) {
				var outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': 'session found',
					'data': req.session.user

				}
				return res.send(outputJSON);
			} else {
				var outputJSON = {
					'status': 'failure',
					'messageId': 400,
					'message': 'Session Expired! Please try again.',
					'data': req.session.user

				}
				return res.send(outputJSON);
			}

		} catch (e) {

		}
	}
	//END


//Function to implement logOut
exports.logOut = function(req, res) {
		try {
			delete req.session.user
			if (!req.session.user) {
				var outputJSON = {
					'status': 'success',
					'messageId': 400,
					'message': 'User logout successfully!! Session destroyed !',
					'data': req.session

				}
				return res.send(outputJSON);
			}

		} catch (e) {

		}
	}
	//END

//Function to implement forgotPassword
exports.forgotPassword = function(req, res) {
	try {
		var email = req.body.email;

		var encryted_data = {};
		sellerLoginDB.findOne({
			email: email
		}, function(err, details) {

			if (details == null) {
				var response = {
					"status": 'faliure',
					"messageId": 401,
					"message": "Email does not exist."
				};
				res.status(401).json(response);
			} else {
				var email_encrypt = encrypt(details.email);

				var generatedText = makeid();


				if (email_encrypt && generatedText) {

					var filter = {
						"email": details.email
					}
					sellerLoginDB.update(filter, {
						$set: {
							"randomstring": generatedText
						}
					}, {
						upsert: true
					}, function(err, data) {

						if (err) {
							response = {
								"status": 'error',
								"messageId": 400,
								"message": "Please try agian.",
								"error": err
							};
							res.status(400).json(response);
						} else {

							var resetUrl = "http://" + req.headers.host + "/#/" + "resetPassword/" + email_encrypt + "/" + generatedText;
							console.log("resetUrl",resetUrl);

							var message = '<html><body style="background-color: #f2f2f2"><div style="width:90%; padding: 15px; background-color: #fff; box-shadow: 3px 3px #dddddd;"><div style="padding-top:10px; background-color: #f0f0f0; height: 100px"><img src="http://' + req.headers.host + '/logo.png" height="60px" style="padding: 15px"></div><div style="padding-top:10px">Hi,</div><div style="padding-top:30px">This email was sent automatically by OCR APP in response to your request to recover your password.</div><div style="padding-top:20px">If you did not initiate this request, then ignore this message.</div><div style="padding-top:20px">To reset your password and access your account, click on the following link :</div><div style="padding-top:30px"><a href="' + resetUrl + '">Reset Password</a></div><div style="padding-top:50px">Thanks & Regards,<br>OCR APP</div></div></body></html>'


							var newdetails = {}
							newdetails._id = details._id;
							newdetails.randomstring = details.randomstring;
							newdetails.resetUrl = resetUrl;
							newdetails.email = email;
							newdetails.message = message;

							emails("ForgotPassword", details.email, newdetails, function(error, success) {
								if (error) {
									console.log("error",error);
									response = {
										"status": 'error',
										"messageId": 400,
										"message": "Error sending email!!",
										"error": error
									};
									return res.status(400).json(response);
								} else {
                                    console.log("success",success);
									response = {
										"status": 'success',
										"messageId": 200,
										"message": "Email sent successfully!!",
										"success": success
									};
									return res.status(200).json(response);

								}
							})
						}
					})

				}

			}
		})



	} catch (e) {

	}
}

//END

//Function to implement reset Password
exports.resetPassword = function(req, res) {

		var details = req.body;

		var email = decrypt(req.body.email);
		var newPassword = req.body.password;
		var confirmPassword = req.body.confirm_password;
		var random_string = req.body.randomcode;
		if (newPassword == confirmPassword) {

			var filter = {
				"email": email,
				"randomstring": random_string
			}

        var newPass = jsmd5(newPassword);
			sellerLoginDB.update(filter, {
				$set: {
					"password": newPass
				}
			}, function(error, updateResults) {
				if (error) {
					console.log("error",error);
					var response = {
						"status": 'faliure',
						"messageId": 401,
						"message": "Error updating Password!!"
					};
					res.status(401).json(response);
				} else {
					console.log("updateResults", updateResults);
					var response = {
						"status": 'success',
						"messageId": 200,
						"message": "Password updated successfully!!"
					};
					res.status(200).json(response);
				}

			})
		} else {
			var response = {
				"status": 'failure',
				"messageId": 401,
				"message": "Password and confirm password does not match."
			};
			res.status(401).json(response);
		}


	}




	exports.getSettingsData = function(req,res){
		if(req.session.user){
			var id = req.session.user.user._id;
		   sellerLoginDB.findOne({_id:id}).exec(function(err,response){
				    if(err){
							var response = {
								"status": 'failure',
								"messageId": 400,
								"message": "Problem retrieving data"
							};
							res.status(401).json(response);
						}else{
							var response = {
								"status": 'success',
								"messageId": 200,
								"message": "Data retrieved successfully.",
								"data":response
							};
							res.status(200).json(response);
						}
			 })
		}

	}
	//END
