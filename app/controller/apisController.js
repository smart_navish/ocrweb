var users = require('./../model/usersModel.js');
var languages = require('./../model/languageModel.js');
var plans = require('./../model/planModel.js');
var subsriptions = require('./../model/subscriptionModel.js');
var emails = require('./../config/emails/emailTemplate.js');
var md5 = require('md5');
var config = require('./../config/config.js');


var jwt = require('jsonwebtoken');



exports.allLanguage = function(req, res) {
    languages.find({}, function(err, result) {
        if(!err){
            res.send({success: true, data: result});
        }else {
          res.send({success: false, data: []});
        }

    })
}

exports.allSubscriptions = function(req, res) {
    plans.find({}, function(err, result) {
        if(!err){
            res.send({success: true, data: result});
        }else {
          res.send({success: false, data: []});
        }

    })
}

/* Registration of end user 20-09-2016 By Navish */

exports.registerUser = function (req, res) {
    console.log('In registerUser');
    var userInfo = {};
    userInfo.email = req.body.email;
    userInfo.password = md5(req.body.password);
    userInfo.fullName = req.body.fullName;
    userInfo.phoneNumber = req.body.phoneNumber;
    userInfo.language = (req.body.language_id == "") ? null : req.body.language_id;
    userInfo.confirmationToken = config.random(5);
    //languages.create({name: "abc"});
    console.log('body-------', JSON.stringify(req.body));
    users.findOne({email: userInfo.email}, function (err, data) {
        if(!err){
            console.log('userExist and count is ', data);
            if(data)
            {
                if(data.verified === true)
                {
                     console.log('User is exist');
                     res.send({success: false, message: 'User already exist'});
                }
                else
                {
                    console.log('Please confirm your account first.');
                    res.send({success: false, message: 'Please confirm your account first.'});
                }
            }
            else
            {
                users(userInfo).save(function (err, insertdata) {
                    if(!err)
                    {
                        console.log('data insert', insertdata);
                        userInfo.logo = req.protocol + "://" + req.headers.host + "/images/logo.png";
                        emails('RegisterCode', req.body.email, userInfo, function (errors, responses) {
                          res.send({success: true, message: 'Successfully authenticate!', userID: insertdata._id, token : userInfo.confirmationToken});
                        });

                    }
                    else
                    {
                        console.log('error while insert', err);
                        throw err;
                    }
                });
            }
        }
        else
        {
            console.log('error while find the user', err);
            throw err;
        }
    });
};

exports.confirmationEmail = function (req, res) {
    console.log('In confirmationEmail');
    var fetchInfo = {};
    fetchInfo._id = req.body.userId;
    fetchInfo.confirmationToken = req.body.confirmationToken;
    var updateInfo = {};
    var setInfo = {};
    setInfo.verified = true;

    users.findOneAndUpdate(fetchInfo,{$set:setInfo}, {}, function(err, data){
       if(!err)
       {
           if(data)
           {
                 updateInfo.user_id = data._id;
                 updateInfo.plan_id = "Free";
                 updateInfo.subscription_price = 0.00;
                 updateInfo.listed_end_date = Date.now() + 3 * (24*60*60*1000);

                 subscribefunction(updateInfo, function(err, result) {
                     if(!err){
                       console.log('record found and update data ',data);
                       res.send({success: true, message: 'Successfully authenticate!', data: data});
                     }else {
                       res.send({success: true, message: 'Successfully authenticate but free subscription not updated!', data: data});
                     }
                 })
           }
           else
           {
                console.log('record not found and update data ',data);
                res.send({success: false, message: 'Sorry, problem to authenticate!', data: data});
           }
       }
       else
       {
           console.log('error while updating', err);
           throw err;
       }
    });
};

exports.login = function(req, res)
{
    console.log('In login section');
    var fetchInfo = {};
    fetchInfo.email = req.body.email;
    fetchInfo.password = md5(req.body.password);
    fetchInfo.status = true;
    users.findOne(fetchInfo).populate('language').populate('subscription').exec(function(err, userData){
        if(!err)
        {
            if(userData)
            {
                if(userData.verified === true)
                {
                    console.log('User data', userData);
                    var token = jwt.sign(fetchInfo, config.jwtSecret, {
                            expiresIn: 1460 // expires in 24 hours
                    });
                    var userDetail = {};
                    var dateNow = new Date();
                    userDetail.email = userData.email;
                    userDetail.phoneNumber = userData.phoneNumber;
                    userDetail.fullName = userData.fullName;
                    userDetail.userId = userData._id;
                    userDetail.language = userData.language == null ? '' :userData.language.name;
                    userDetail.language_id = userData.language == null ? '' : userData.language._id;
                    userDetail.language_code = userData.language == null ? '' : userData.language.code;
                    userDetail.token = token;
                    userDetail.subscription = userData.subscription == null ? {} : userData.subscription;
                    userDetail.isExpired =  Math.round((userData.subscription.listed_end_date.getTime() - dateNow.getTime()) / (24*60*60*1000)) >= 0 ? false : true;
                    userDetail.daysleft = Math.round((userData.subscription.listed_end_date.getTime() - dateNow.getTime()) / (24*60*60*1000)) >= 0 ? Math.round((userData.subscription.listed_end_date.getTime() - dateNow.getTime()) / (24*60*60*1000)) : 0;
                    res.send({success: true, message: 'Successfully authenticatea!', token: token, userData : userDetail});
                }
                else
                {
                    console.log('Please confirm your account first.');
                    res.send({success: false, message: 'Please verify your email id first.'});
                }
            }
            else
            {
                console.log('Sorry, no record found');
                res.send({success: false, message: 'Authentication failed. Email and password not found.'});
            }
        }
        else
        {
            console.log('error while fetching informatioin', err);
            throw err;
        }
    });
};


exports.fb_login = function (req, res) {
    console.log('In fb_login');
    var userInfo = {};
    var updateInfo = {};
    var userDetail = {};
    var dateNow = new Date();
    userInfo.email = (req.body.email != '' ? req.body.email : '');
    userInfo.fullName = req.body.fullName;
    userInfo.phoneNumber = (req.body.phoneNumber != '' ? req.body.phoneNumber : '');
    userInfo.language = (req.body.language_id == "") ? null : req.body.language_id;
    userInfo.fb_id = req.body.fb_id;
    userInfo.verified = true;
    //languages.create({name: "abc"});
    console.log('body-------', JSON.stringify(req.body));
    users.findOne({fb_id: userInfo.fb_id}).populate('language').populate('subscription').exec(function (err, userData) {
        if(!err){
            console.log('userExist and count is ', userData);
            if(userData)
            {
                  var token = jwt.sign(userInfo, config.jwtSecret, {
                          expiresIn: 1460 // expires in 24 hours
                  });

                  userDetail.email = userData.email;
                  userDetail.fb_id = userData.fb_id;
                  userDetail.phoneNumber = userData.phoneNumber;
                  userDetail.fullName = userData.fullName;
                  userDetail.userId = userData._id;
                  userDetail.language = userData.language ? userData.language.name : '';
                  userDetail.language_id = userData.language == null ? '' :userData.language._id;
                  userDetail.language_code = userData.language == null ? '' : userData.language.code;
                  userDetail.subscription = userData.subscription == null ? {} : userData.subscription;
                  userDetail.token = token;
                  userDetail.isExpired =  Math.round((userData.subscription.listed_end_date.getTime() - dateNow.getTime()) / (24*60*60*1000)) >= 0 ? false : true;
                  userDetail.daysleft = Math.round((userData.subscription.listed_end_date.getTime() - dateNow.getTime()) / (24*60*60*1000)) >= 0 ? Math.round((userData.subscription.listed_end_date.getTime() - dateNow.getTime()) / (24*60*60*1000)) : 0;
                  res.send({success: true, message: 'Successfully authenticate!', token: token, userData : userDetail});
            }
            else
            {
                users(userInfo).save(function (err, insertdata) {
                    if(!err)
                    {
                        updateInfo.user_id = insertdata._id;
                        updateInfo.plan_id = "Free";
                        updateInfo.subscription_price = 0.00;
                        updateInfo.listed_end_date = Date.now() + 3 * (24*60*60*1000);

                        subscribefunction(updateInfo, function(errs, results) {
                            if(!errs){
                                userInfoFun(userInfo.fb_id, userInfo, function(rerr, rresult){
                                    if(!rerr){
                                        res.send({success: true, message: 'Successfully authenticate!', token: rresult.token, userData : rresult});
                                    }else {
                                        res.send({success: false, message: 'Sorry! Problem to fetch the record.'});
                                    }
                                })
                            }else {
                                userInfoFun(userInfo.fb_id, userInfo, function(rerr, rresult){
                                    if(!rerr){
                                        res.send({success: true, message: 'Successfully authenticate but free subscription not updated!', token: rresult.token, userData : rresult});
                                    }else {
                                        res.send({success: false, message: 'Sorry! Problem to fetch the record.'});
                                    }
                                })
                            }
                        })
                    }
                    else
                    {
                        console.log('error while insert', err);
                        throw err;
                    }
                });
            }
        }
        else
        {
            console.log('error while find the user', err);
            throw err;
        }
    });
};

var userInfoFun =  function(user_fb_Id, userInfo, calb) {
  var userDetail = {};
  users.findOne({fb_id: user_fb_Id}).populate('language').populate('subscription').exec(function (uerr, userData) {
      if(!uerr){
          if(userData)
          {
                var token = jwt.sign(userInfo, config.jwtSecret, {
                        expiresIn: 1460 // expires in 24 hours
                });
                var dateNow = new Date();
                userDetail.fb_id = userData.fb_id;
                userDetail.phoneNumber = userData.phoneNumber;
                userDetail.fullName = userData.fullName;
                userDetail.userId = userData._id;
                userDetail.language = userData.language ? userData.language.name : '';
                userDetail.language_id = userData.language == null ? '' :userData.language._id;
                userDetail.language_code = userData.language == null ? '' : userData.language.code;
                userDetail.subscription = userData.subscription == null ? {} : userData.subscription;
                userDetail.token = token;
                userDetail.isExpired =  Math.round((userData.subscription.listed_end_date.getTime() - dateNow.getTime()) / (24*60*60*1000)) >= 0 ? false : true;
                userDetail.daysleft = Math.round((userData.subscription.listed_end_date.getTime() - dateNow.getTime()) / (24*60*60*1000)) >= 0 ? Math.round((userData.subscription.listed_end_date.getTime() - dateNow.getTime()) / (24*60*60*1000)) : 0;
                calb(null, userDetail);
          }else {
            console.log('Sorry, Problem to get data');
            calb('error', null);
          }
        }else {
          calb(uerr, null);
        }
  });
}


exports.updateProfile = function(req, res)
{
    console.log('In update profile', req.body);
    var fetchInfo = {};

    fetchInfo._id = req.body.userId;
    var userInfo = {};

    userInfo.fullName = req.body.fullName;
    userInfo.phoneNumber = req.body.phoneNumber;
    if(req.body.language_id != ""){
        userInfo.language = req.body.language_id;
    }
    users.findOne(fetchInfo, function(err, userData){
        if(!err)
        {
            if(userData)
            {
                users.update(fetchInfo, {$set: userInfo}, function(resErr, resData){
                    if(!resErr)
                    {
                        res.send({success: true, message: 'Successfully updated!', data: resData});
                    }
                    else
                    {
                        res.send({success: false, message: 'Error while updating informatioin!', data: resErr});
                    }

                });
            }
            else
            {
                res.send({success: false, message: 'Sorry, no record found.'});
            }
        }
        else
        {
            res.send({success: false, message: 'Error while fetching informatioin', data: err});
            console.log('Error while fetching informatioin', err);
        }
    });
};

exports.resetPassword = function(req, res)
{
    console.log('In change password');
    var fetchInfo = {};
    var userInfo = {};

    fetchInfo._id = req.body.userId;
    userInfo.password = md5(req.body.password);

    users.findOne(fetchInfo, function(err, userData){
        if(!err)
        {
            if(userData)
            {
                users.update(fetchInfo, {$set: userInfo}, function(resErr, resData){
                    if(!resErr)
                    {
                        res.send({success: true, message: 'Successfully updated!', data: resData});
                    }
                    else
                    {
                        res.send({success: false, message: 'Error while updating informatioin', data: resErr});
                    }

                });
            }
            else
            {
                res.send({success: false, message: 'Sorry, no record found.'});
            }
        }
        else
        {
            res.send({success: false, message: 'Error while fetching informatioin', data: err});
            console.log('Error while fetching informatioin', err);
        }
    });
};

exports.changePassword = function(req, res)
{
    console.log('In change password');
    var fetchInfo = {};
    var userInfo = {};
    fetchInfo._id = req.body.userId;
    fetchInfo.password = md5(req.body.oldpassword);

    users.findOne(fetchInfo, function(err, userData){
        if(!err)
        {
            if(userData)
            {
                userInfo._id = req.body.userId;
                userInfo.password = md5(req.body.newpassword);
                users.update(fetchInfo, {$set: userInfo}, function(resErr, resData){
                    if(!resErr)
                    {
                        res.send({success: true, message: 'Successfully updated!', data: resData});
                    }
                    else
                    {
                        res.send({success: false, message: 'Error while updating informatioin', data: resErr});
                    }
                });
            }
            else
            {
                res.send({success: false, message: 'Sorry, no record found. Please check your old password!'});
            }
        }
        else
        {
            res.send({success: false, message: 'Error while fetching informatioin', data: err});
            console.log('Error while fetching informatioin', err);
        }
    });
};


exports.profileDetail = function(req, res)
{
    console.log('In profileDetail');
    var fetchInfo = {};

    fetchInfo._id = req.body.userId;

    users.findOne(fetchInfo, function(err, userData){
        if(!err)
        {
            if(userData)
            {
                //userDetail.email = req.body.email, 'bcryptr');
                userData.userId = userData._id;
                res.send({success: true, message: 'Users data.', userData: userData});
            }
            else
            {
                res.send({success: false, message: 'Sorry, no record found.'});
            }
        }
        else
        {
            res.send({success: false, message: 'Error while fetching informatioin', data: err});
            console.log('Error while fetching informatioin', err);
        }
    });
};

exports.forgotPassword = function(req, res)
{
    console.log('In forgotPassword');
    var fetchInfo = {};
    var userInfo = {};
    var setInfo = {};
    fetchInfo.email = req.body.email;
    setInfo.forgotToken = config.random(5);


    users.findOneAndUpdate(fetchInfo, {$set: setInfo}, {}, function(err, userData){
        if(!err)
        {
            if(userData)
            {
                if(userData.verified === true)
                {
                    userInfo.ForgotOTP = setInfo.forgotToken;
                    userInfo.logo = req.protocol + "://" + req.headers.host + "/images/logo.png";
                    emails('ForgotPassword', req.body.email, userInfo, function (err, responseData) {
                        if(!err){
                            res.send({success: true, message: 'Link successfully sent to your email id.', token: userInfo.ForgotOTP});
                        }else {
                            res.send({success: false, message: 'Sorry, Proble to send email, Please try again later'});
                        }
                    });

                }
                else
                {
                    //console.log('Please confirm your account first.');
                    res.send({success: false, message: 'Please confirm your account first.'});
                }
            }
            else
            {
                res.send({success: false, message: 'Sorry, no record found.'});
            }
        }
        else
        {
            res.send({success: false, message: 'Error while fetching informatioin', data: err});
            console.log('error while fetching informatioin', err);
        }
    });

};

exports.forgotPasswordConfirmation = function (req, res) {
    console.log('In forgotPasswordConfirmation');
    var fetchInfo = {};

    fetchInfo.forgotToken = req.body.forgotToken;

    var setInfo = {};
    setInfo.password = md5(req.body.newPassword);
    setInfo.forgotToken = "";

    users.findOneAndUpdate(fetchInfo, {$set:setInfo}, {}, function(err, data){
       if(!err)
       {
           if(data)
           {
                console.log('record found and update data ',data);
                res.send({success: true, message: 'Successfully authenticate!', data: data});
           }
           else
           {
                console.log('record not found and update data ',data);
                res.send({success: false, message: 'Sorry, problem to authenticate!', data: data});
           }
       }
       else
       {
           console.log('error while updating', err);
           throw err;
       }
    });
};

//Only for temprary as discuss with JATIN
  exports.tokenConfirmation = function (req, res) {
    console.log('In tokenConfirmation');
    var fetchInfo = {};

    fetchInfo.forgotToken = req.body.forgotToken;

    var setInfo = {};
    setInfo.forgotToken = "";

    users.findOneAndUpdate(fetchInfo, {$set:setInfo}, {}, function(err, data){
       if(!err)
       {   console.log('--------------  ', data);
           if(data)
           {
             var token = jwt.sign(fetchInfo, config.jwtSecret, {
                     expiresIn: 1460 // expires in 24 hours
             });
                console.log('record found and update data ',data);
                res.send({success: true, message: 'Successfully authenticate!', token : token, data: data});
           }
           else
           {
                console.log('record not found and update data ',data);
                res.send({success: false, message: 'Sorry, problem to authenticate!', data: data});
           }
       }
       else
       {
           console.log('error while updating', err);
           throw err;
       }
    });
};


//add scription

exports.addSubscription = function(req, res)
{
    console.log('In change add subsriptions');
    var updateInfo = {};

    updateInfo.user_id = req.body.userId;
    updateInfo.plan_id = req.body.plan_id;
    updateInfo.subscription_price = req.body.subscription_price;
    updateInfo.listed_end_date = Date.now()  + req.body.daysLeft * (24*60*60*1000);

    subscribefunction(updateInfo, function(err, result) {
        if(!err){
          res.send({success: true, message: 'Subscription successfully added!', data: result});
        }else {
          res.send({success: false, message: 'Error while updating informatioin', data: err});
        }
    })

};


var subscribefunction = function (updateInfo, cb) {
  subsriptions(updateInfo).save(function (err, response) {
        if(!err){
          console.log("responsedata", JSON.stringify(response));
          users.update({"_id": updateInfo.user_id}, {$set : {"subscription" : response._id}}, function (uerr, uresponse) {
              if(!uerr){
                cb(null, response);
              }else {
                cb(uerr, null);
              }
          })
        }else {
          cb(err, null);
        }
  })
}
