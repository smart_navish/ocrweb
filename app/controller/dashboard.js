var mongoose = require('mongoose');

var purchaseModel = require('./../model/usersModel.js');

var moment = require("moment")

exports.orderDetail = function(req, res) {
  var endDate = moment();
  if (req.body.Datatype == 'month') {
    var startDate = moment().subtract(1, "months").startOf("day");
    console.log("startDate", startDate);
  } else {
    var startDate = moment().subtract(7, "days").startOf("day");
    console.log("startDate", startDate);
  }
  if (req.body.labelType == "Shopify") {
    var label = ["Shopify"]
  } else if (req.body.labelType == "eBay") {
    var label = ["eBay"]
  } else if (req.body.labelType == "Amazon") {
    var label = ["Amazon"]
  } else {
    var label = ["Shopify", "eBay", "Amazon"]
  }

  var query = {
    "orderCreatedDate": {
      $gte: startDate.toDate(),
      $lt: endDate.toDate()
    },
    label: {
      $in: label
    }
  }



  console.log("query ", JSON.stringify(query));

  orderModel.aggregate([{
    $match: query
  }, {
    $project: {
      total_price: 1,
      netProfit: 1,
      orderCreatedDate: 1
    }
  }, {
    $group: {
      "_id": {
        month: {
          $month: "$orderCreatedDate"
        },
        day: {
          $dayOfMonth: "$orderCreatedDate"
        },
        year: {
          $year: "$orderCreatedDate"
        }
      },
      total_price: {
        "$sum": "$total_price"
      },
      netProfit: {
        "$sum": "$netProfit"
      },
      orderCreatedDate: {
        "$first": "$orderCreatedDate"
      }
    }
  }, {
    $sort: {
      _id: 1
    }
  }]).exec(function(err, data) {

    if (err) {
      console.log('err = ',err);
      var outputJSON = {
        'status': 'failure',
        'messageId': 400,
        'message': 'Records not found',
      }
      res.send(outputJSON);
    } else {
      console.log('data = ',data);
      if (data.length > 0) {

       // console.log("data",data.length,"here#######################################",data);
        var netprofit = []
        var totalPrice = [];
        var finalrecord = [];
        var dates = [];

        data.forEach(function(data1) {
          netprofit.push(data1.netProfit);

          totalPrice.push(data1.total_price);

          dates.push(moment(data1.orderCreatedDate).utc().format("YYYY-MM-DD"));
          // dates.push(data1.orderCreatedDate);

        })

        finalrecord.push(netprofit, totalPrice);
        console.log("finalrecord",finalrecord);

        var outputJSON = {
          'status': 'success',
          'messageId': 200,
          'message': 'Final data ',
          'finalrecord': {
            finalrecord,
            dates
          }


        }
        res.send(outputJSON);
      } else {
        var outputJSON = {
          'status': 'failure',
          'messageId': 400,
          'message': 'Record not found',
        }
        res.send(outputJSON);
      }
    }

  })
}

exports.revenueDetail = function(req, res) {

  var orderCreatedDate = moment().startOf("day");
  console.log("orderCreatedDate",orderCreatedDate);
  var orderDayEnd = moment().endOf("day");
  console.log("orderDayEnd",orderDayEnd);

  // console.log("orderCreatedDate", orderCreatedDate.toDate(), "<<<<>>>>", orderDayEnd.toDate());

  orderModel.aggregate([{
    $match: {
      "orderCreatedDate": {
        $gte: orderCreatedDate.toDate(),
        $lt: orderDayEnd.toDate()
      }
    }
  }, {
    $group: {
      "_id": {
        month: {
          $month: "$orderCreatedDate"
        },
        day: {
          $dayOfMonth: "$orderCreatedDate"
        },
        year: {
          $year: "$orderCreatedDate"
        }
      },
      total_price: {
        $sum: "$total_price"
      },
      netProfit: {
        $sum: "$netProfit"
      },
      orderStatus: {
        $sum: {
          $cond: [{
            $or: [{
              $eq: ['$orderStatus', "fulfilled"]
            }, {
              $eq: ['$orderStatus', "Completed"]
            }]
          }, 1, 0]
        }
      }
    }
  }]).exec(function(err, data) {
    if (err) {

    } else {
      console.log("<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>",JSON.stringify(data));
      marginTotal(res, data);
    }
  })
}

var marginTotal = function(res, revenueData) {
  var orderCreatedDate = moment().startOf("day");
  var orderDayEnd = moment().endOf("day");
  orderModel.aggregate([{
    $match: {
      "orderCreatedDate": {
        $gte: orderCreatedDate.toDate(),
        $lt: orderDayEnd.toDate()
      }

    }
  }, {
    $project: {
      total_price: 1,
      netProfit: 1,
      orderCreatedDate: 1,
      margin: 1,

      isequal: {
        $ne: ['$margin', 100]
      }
    }
  }, {
    $match: {
      isequal: true
    }
  }, {
    $group: {
      "_id": {
        month: {
          $month: "$orderCreatedDate"
        },
        day: {
          $dayOfMonth: "$orderCreatedDate"
        },
        year: {
          $year: "$orderCreatedDate"
        }
      },
      count: {
        $sum: 1
      },
      margin: {
        $sum: "$margin"
      },
    }
  }, {
    $project: {
      margin: {
        $divide: ["$margin", "$count"]
      }
    }
  }, ]).exec(function(err, marginData) {

    inventoryModel.find({
      $where: "this.qty > this.amazonFBAWareHouseQty"
    }).count().exec(function(err, lowstock) {
      var outputJSON = {
        'status': 'success',
        'messageId': 200,
        'message': 'Final data ',
        'finalrecord': {
          marginData,
          revenueData,
          lowstock
        }

      }

      res.send(outputJSON);
    })

  })
}


/**********pening stock ********/
exports.pendingStock = function(req, res) {

  purchaseModel.find({
    "order_status": "In Progress"
  }).sort("-createdOn").limit(10).exec(function(err, doc) {
    if (!err) {

      var outputJSON = {};
      outputJSON.status = 'success';
      outputJSON.messageId = 200;
      outputJSON.message = 'Last 10 records of Pending Stock Succeed';
      outputJSON.data = doc;
      res.json(outputJSON);
    } else {
      var outputJSON = {};
      outputJSON.status = 'failure';
      outputJSON.messageId = 400;
      outputJSON.message = 'Something is wrong';
      outputJSON.data = err;
      res.json(outputJSON);
    }
  });
}

exports.lowStock = function(req, res) {

  inventoryModel.aggregate([{
    $project: {
      "item": 1,
      "description": 1,
      "sku": 1,
      "qty": 1,
      "avgUnitCost": 1,
      "supplier": 1,
      "category": 1,
      "subCategory": 1,
      "amazonFBAWareHouseQty": 1,
      "wareHouse": 1,
      "totalValue": {
        $sum: ["$wareHouse.Warehouse Qty 5", "$wareHouse.Warehouse Qty 4", "$wareHouse.Warehouse Qty 3", "$wareHouse.Warehouse Qty 2", "$wareHouse.Warehouse Qty 1"]
      }
    }
  }, {
    $sort: {
      createdOn: -1
    }
  }, {
    $skip: 0
  }, {
    $limit: 10
  }]).exec(function(err, doc) {
    if (!err) {

      var outputJSON = {};
      outputJSON.status = 'success';
      outputJSON.messageId = 200;
      outputJSON.message = 'Last 10 records of Low Stock Succeed';
      outputJSON.data = doc;
      res.json(outputJSON);
    } else {
      var outputJSON = {};
      outputJSON.status = 'failure';
      outputJSON.messageId = 400;
      outputJSON.message = 'Something is wrong';
      outputJSON.data = err;
      res.json(outputJSON);
    }
  })
}


exports.getDashboard = function(req, res) {
  if (req.session.user) {
    var filter = {};
    purchase.aggregate([
      //
      {
        $project: {
          total_PO: 1,
          total_purchase_order: {
            $sum: 1
          },
          unit_orders: {
            $sum: "$purchaseOrderDetails.quantity"
          },
          unit_recevied: {
            $sum: "$purchaseOrderDetails.received_quantity"
          },
          order_status: {
            $cond: [{
              $eq: ['$order_status', "In Progress"]
            }, 1, 0]
          },
          closed_orders: {
            $cond: [{
              $eq: ['$order_status', "Complete"]
            }, 1, 0]
          },
        }

      }, {
        $group: {
          _id: null,
          total_PO: {
            $sum: "$total_PO"
          },
          total_purchase_order: {
            $sum: 1
          },
          Inprogress_order: {
            $sum: "$order_status"
          },
          closed_orders: {
            $sum: "$closed_orders"
          },
          unit_orders: {
            $sum: "$unit_orders"
          },
          unit_received: {
            $sum: "$unit_recevied"
          }
        }
      }
    ]).exec(function(err, doc) {
      if (err) {
        var outputJSON = {};
        outputJSON.status = 'failure';
        outputJSON.messageId = 400;
        outputJSON.message = 'Something is wrong';
        outputJSON.data = err;
        res.json(outputJSON);
      } else {
        //console.log("Response Data from purchaseModel is : ");
        res.json({
          message: "Successfully get Purchased data",
          data: doc
        });
      }
    }); //Exec Function Completed................

  } else {
    var outputJSON = {};
    outputJSON.status = 'failure';
    outputJSON.messageId = 400;
    outputJSON.message = 'Please login first to sync!';
    outputJSON.data = 'Not login yet';
    res.json(outputJSON);
  }

}

exports.inventoryStock = function(req, res) {
  //console.log("Final API for Get Inventory Stock");
  inventoryModel.aggregate([{
    $group: {
      _id: "$category",
      Total_Quantity: {
        $sum: "$qty"
      },
      Unit_Cost: {
        $sum: "$avgUnitCost"
      },
      total_value: {
        $sum: {
          $multiply: [

            "$qty", "$avgUnitCost"



          ]
        }
      }
    }
  }]).exec(function(err, doc) {
    if (!err) {
      //console.log("Data Successfully get.... No error");
      var outputJSON = {};
      outputJSON.status = 'success';
      outputJSON.messageId = 200;
      outputJSON.message = 'Inventory Stock Succeed';
      outputJSON.data = doc;
      res.json(outputJSON);
    } else {
      var outputJSON = {};
      outputJSON.status = 'failure';
      outputJSON.messageId = 400;
      outputJSON.message = 'Something is wrong';
      outputJSON.data = err;
      res.json(outputJSON);
    }
  });
}
