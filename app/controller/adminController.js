var adminDB = require("./../model/admin.js");
var sellerLoginDB = require("./../model/sellerModel.js");
var amazonShippingDB = require("./../model/amazonShippingCreditModel.js");
var amazonReferralDB = require("./../model/amazonReferralFeeModel.js");
var emails = require("./../config/emails/emailTemplate.js");
var subscriptionDB = require('./../model/subscriptionModel.js');
var ebayFeesDB = require('./../model/ebayFeeModel.js');
var ebayUpgradeFeesDB = require('./../model/ebayUpgradeFeeModel.js');
var jsmd5 = require('js-md5');

//Encryption And Decryption
var crypto = require('crypto'),
	algorithm = 'aes-256-ctr',
	password = 'd6F3Efeq';
encrypt = function(text) {
	var cipher = crypto.createCipher(algorithm, password)
	var crypted = cipher.update(text, 'utf8', 'hex')
	crypted += cipher.final('hex');
	return crypted;
}
decrypt = function(text) {
	var decipher = crypto.createDecipher(algorithm, password)
	var dec = decipher.update(text, 'hex', 'utf8')
	dec += decipher.final('utf8');
	return dec;
}

makeid = function() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < 15; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	}
	//END


exports.login = function(req, res) {
	try {
		req.session.admin = res.req.user;
			//req.session.admin = res.req.user;
			console.log("*-*--*-*-*-admin*-*-*-*-*",req.session.admin)
		if (res.req.user) {
			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'login successfully',
				'data': res.req.admin

			}
			return res.send(outputJSON);
		}

	} catch (e) {

	}
}


//Function to implement logOut
exports.logOut = function(req, res) {

		try {
			console.log(req.session)
			delete req.session.admin
			if (!req.session.admin) {
				var outputJSON = {
					'status': 'success',
					'messageId': 400,
					'message': 'User logout successfully!! Session destroyed !',
					'data': req.session

				}
				return res.send(outputJSON);
			}

		} catch (e) {

		}
	}
	//END


//Function to Check Session
exports.checkSession = function(req, res) {
		try {
console.log("*-*--*-*-*-admin*-*-*-*-*",req.session.admin)
			if (req.session.admin) {
				var outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': 'session found',
					'data': req.session.admin

				}
				return res.send(outputJSON);
			} else {
				console.log("m here in else")
				var outputJSON = {
					'status': 'failure',
					'messageId': 400,
					'message': 'Session Expired! Please try again.',
					'data': req.session.admin

				}
				return res.send(outputJSON);
			}

		} catch (e) {

		}
	}
	//END



exports.userlist = function(req, res) {
	try {
		var page = req.body.page || 1,
			count = req.body.count || 1;
		var searchQry = req.body.search;


		var sortkey = null;
		for (key in req.body.sort) {
			console.log("key", key);
			sortkey = key;
		}
		if (sortkey) {
			var sortquery = {};
			sortquery[sortkey ? sortkey : 'createdOn'] = req.body.sort ? (req.body.sort[sortkey] == 'desc' ? -1 : 1) : -1;
		}

		var filter = {};
		filter.$and = [{
			"isDeleted": false
		}];



		if (req.body.search) {
			filter.$or = [{
				username: {
					$regex: searchQry,
					$options: 'i'
				}
			}, {
				email: {
					$regex: searchQry,
					$options: 'i'
				}
			}, {
				role: {
					$regex: searchQry,
					$options: 'i'
				}
			}, {
				status: {
					$regex: searchQry,
					$options: 'i'
				}
			}]

		}



		var skipNo = (page - 1) * count;

		sellerLoginDB.find().and(filter).sort(sortquery).skip(skipNo).limit(count).exec(function(error, result) {
			if (error) {
				var outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': 'data not retrieved ',
					'err': err
				};
				return res.send(outputJSON);
			} else {

				sellerLoginDB.find(filter).count().exec(function(err, total) {

					var outputJSON = {
						'status': 'success',
						'messageId': 200,
						'message': 'data retrieve from products',
						'data': result,
						'count': total
					}
					return res.send(outputJSON);
				})


			}
		})


	} catch (e) {
		console.log("exception", e);
	}
}

//Amazon
//Function to show amazon shipping credits
exports.getAmazonShippingCredit = function(req, res) {
	try {
		var searchQry = req.body.search;
		console.log("The searchQry is : ", searchQry);
		var filter = {};
		if (req.body.search) {
			filter.$or = [{
				name: {
					$regex: searchQry,
					'$options': 'i'
				}
			}]
		}

		amazonShippingDB.find(filter).exec(function(err, response) {
			if (err) {
				console.log(err);
				var outputJSON = {
					'status': 'failure',
					'messageId': 400,
					'message': 'Problem retrieving data',
					'err': err
				};
				return res.send(outputJSON);
			} else {
				console.log("Responseee : ", response);
				var outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': 'Successfull Retrieval',
					'data': response
				};
				return res.send(outputJSON);
			}
		})
	} catch (e) {

	}
}

exports.updateAmazonCredit = function(req, res) {
	var id = req.body._id;
	console.log("id", id);
	var data = req.body;
	console.log("Print data", data);
	var filter = {
		"_id": id
	}
	if (data.Domestic_Standard) {
		if (data.Domestic_Standard.price && data.Domestic_Standard.perlb) {
			data.Domestic_Standard.price = parseFloat(data.Domestic_Standard.price);
			data.Domestic_Standard.perlb = parseFloat(data.Domestic_Standard.perlb);

		} else {
			data.Domestic_Standard = parseFloat(data.Domestic_Standard);
		}
	}

	if (data.Domestic_Expedited) {
		if (data.Domestic_Expedited.price && data.Domestic_Expedited.perlb) {
			data.Domestic_Expedited.price = parseFloat(data.Domestic_Expedited.price);
			data.Domestic_Expedited.perlb = parseFloat(data.Domestic_Expedited.perlb);

		} else {
			data.Domestic_Expedited = parseFloat(data.Domestic_Expedited);
		}
	}


	if (data.International_Standard) {
		if (data.International_Standard.Up_To_1_LB && data.International_Standard.Over_1_LB) {
			data.International_Standard.Up_To_1_LB = parseFloat(data.International_Standard.Up_To_1_LB);
			data.International_Standard.Over_1_LB = parseFloat(data.International_Standard.Over_1_LB);
		} else {
			data.International_Standard = parseFloat(data.International_Standard);
		}
	}



	var updatedData = {
		"name": data.name,
		"Domestic_Standard": data.Domestic_Standard,
		"Domestic_Expedited": data.Domestic_Expedited,
		"Domestic_Two_Day": parseFloat(data.Domestic_Two_Day),
		"Domestic_One_Day": parseFloat(data.Domestic_One_Day),
		"International_Expedited": parseFloat(data.International_Expedited),
		"International_Standard": data.International_Standard
	}

	amazonShippingDB.update(filter, updatedData).exec(function(error, result) {
		if (error) {
			var outputJSON = {
				'status': 'failure',
				'messageId': 400,
				'message': 'Problem updating data',
				'err': error
			};
			return res.send(outputJSON);
		} else {
			console.log("result", result);
			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'Data updated successfully',
				'data': result
			};
			return res.send(outputJSON);
		}
	})
}


exports.amazonReferralFee = function(req, res) {
	console.log("The aMAZONE rEFEREAL fEE");
	try {
		var page = req.body.page || 1,
			count = req.body.count || 1;
		var searchQry = req.body.search;


		var sortkey = null;
		for (key in req.body.sort) {
			console.log("The keys is : ", key);
			sortkey = key;
		}
		if (sortkey) {
			console.log("The Sort Key is : ", sortkey);
			var sortquery = {};
			sortquery[sortkey ? sortkey : 'name'] = req.body.sort ? (req.body.sort[sortkey] == 'desc' ? -1 : 1) : -1;
		}

		var filter = [{}];
		//var filter = [];
		if (req.body.search) {
			filter.push({
				$or: [{
					name: {
						$regex: searchQry,
						$options: 'i'
					}
				}]
			});
		}
		var skipNo = (page - 1) * count;
		console.log("filterr is : ", JSON.stringify(filter));

		amazonReferralDB.find().and(filter).sort(sortquery).skip(skipNo).limit(count).exec(function(err, response) {
			if (err) {
				var outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': 'data not retrieved ',
					'err': err
				};
				return res.send(outputJSON);
			} else {

				amazonReferralDB.find().and(filter).count().exec(function(err, total) {

					var outputJSON = {
						'status': 'success',
						'messageId': 200,
						'message': 'data retrieved from Amazon Referal Fee',
						'data': response,
						'count': total
					}
					return res.send(outputJSON);
				})
			}
		})


	} catch (e) {
		console.log("exception", e);
	}
}


exports.updateAmazonReferralFee = function(req, res) {
	var data = req.body;

	var id = req.body._id;
	var filter = {
		"_id": id
	}
	var newValues = [];
	var newData = {};

	if (typeof(data.referralFeePercentages) == 'object') {
		data.referralFeePercentages.forEach(function(value, key) {
			if (value.upto) {
				newData = {
					"upto": parseInt(value.upto),
					"from": parseInt(value.from),
					"fee": parseInt(value.fee)
				}
			} else {
				newData = {
					"from": parseInt(value.from),
					"fee": parseInt(value.fee)
				}
			}

			newValues.push(newData);
		});
		var updatedData = {
			"referralFeePercentages": newValues,
			"minimumFee": (data.minimumFee) ? parseInt(data.minimumFee) : ''
		}
	} else {
		var newValueOne = parseInt(data.referralFeePercentages);
		var updatedData = {
			"referralFeePercentages": newValueOne,
			"minimumFee": (data.minimumFee) ? parseInt(data.minimumFee) : ''
		}
	}



	amazonReferralDB.update(filter, updatedData, {
		upsert: true
	}).exec(function(err, response) {
		if (err) {
			var outputJSON = {
				'status': 'failure',
				'messageId': 400,
				'message': 'Problem updating data',
				'err': err
			};
			return res.send(outputJSON);
		} else {

			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'Successfully Updated',
				'data': response
			};
			return res.send(outputJSON);
		}
	})

}



exports.updateSubAdminInfo = function(req, res) {
	var data = req.body;
	console.log("data", data);
	var id = req.body._id;
	var filter = {
		"_id": id
	}

	var updatedData = {
		"username": data.username,
		"email": data.email,
		"first_name": data.first_name,
		"last_name": data.last_name
	};

	adminDB.update(filter, updatedData, {
		upsert: true
	}).exec(function(err, response) {
		if (err) {
			var outputJSON = {
				'status': 'failure',
				'messageId': 400,
				'message': 'Problem updating data',
				'err': err
			};
			return res.send(outputJSON);
		} else {

			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'Successfully Updated',
				'data': response
			};
			return res.send(outputJSON);
		}
	})


}

//END


exports.adminForgotPassword = function(req, res) {
	try {
		var email = req.body.email;

		var encryted_data = {};
		adminDB.findOne({
			email: email
		}, function(err, details) {

			if (details == null) {
				var response = {
					"status": 'faliure',
					"messageId": 400,
					"message": "Email does not exist."
				};
				res.status(400).json(response);
			} else {

				var email_encrypt = encrypt(details.email);
				var generatedText = makeid();


				if (email_encrypt && generatedText) {
					console.log("1233");
					var filter = {
						"email": details.email
					}
					adminDB.update(filter, {
						$set: {
							"randomstring": generatedText
						}
					}, {
						upsert: true
					}, function(err, data) {

						if (err) {
							console.log("err>>>>", err);
							response = {
								"status": 'error',
								"messageId": 400,
								"message": "Please try agian.",
								"error": err
							};
							res.status(400).json(response);
						} else {
							console.log("data", data);
							var resetUrl = "https://" + req.headers.host + "/admin#/" + "resetPassword/" + email_encrypt + "/" + generatedText;

							var message = '<html><body style="background-color: #f2f2f2"><div style="width:90%; padding: 15px; background-color: #fff; box-shadow: 3px 3px #dddddd;"><div style="padding-top:10px; background-color: #f0f0f0; height: 100px"><img src="http://' + req.headers.host + '/logo.png" height="60px" style="padding: 15px"></div><div style="padding-top:10px">Hi,</div><div style="padding-top:30px">This email was sent automatically by Pricing Easy in response to your request to recover your password.</div><div style="padding-top:20px">If you did not initiate this request, then ignore this message.</div><div style="padding-top:20px">To reset your password and access your account, click on the following link :</div><div style="padding-top:30px"><a href="' + resetUrl + '">Reset Password</a></div><div style="padding-top:50px">Thanks & Regards,<br>Pricing Easy</div></div></body></html>'


							var newdetails = {}
							newdetails._id = details._id;
							newdetails.randomstring = details.randomstring;
							newdetails.resetUrl = resetUrl;
							newdetails.email = email;
							newdetails.message = message;

							emails("ForgotPassword", details.email, newdetails, function(error, success) {
								if (error) {
									console.log("error", error);
									response = {
										"status": 'error',
										"messageId": 400,
										"message": "Error sending email!!",
										"error": error
									};
									return res.status(400).json(response);
								} else {
									console.log("success", success);
									response = {
										"status": 'success',
										"messageId": 200,
										"message": "Email sent successfully!!",
										"success": success
									};
									return res.status(200).json(response);

								}
							})
						}
					})

				}

			}
		})



	} catch (e) {

	}
}


//EbaY
exports.getebayFees = function(req, res) {
	try {
		ebayFeesDB.find().exec(function(err, response) {
			if (err) {
				var outputJSON = {
					'status': 'failure',
					'messageId': 400,
					'message': 'Problem updating data',
					'err': err
				};
				return res.send(outputJSON);
			} else {

				var outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': 'Successfully Updated',
					'data': response
				};
				return res.send(outputJSON);
			}
		})
	} catch (e) {

	}
}

exports.updateEbayFee = function(req, res) {
	try {
		var body = req.body;
		console.log("body>>>", req.body);

		var id = req.body._id;
		var fees = req.body.fees;
		var filter = {
			"_id": id
		}

		var updated = {
			"fees": fees
		}

		ebayFeesDB.update(filter, updated).exec(function(error, success) {
			if (error) {
				var outputJSON = {
					'status': 'failure',
					'messageId': 400,
					'message': 'Problem updating data',
					'err': error
				};
				return res.send(outputJSON);
			} else {

				var outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': 'Successfully Updated',
					'data': success
				};
				return res.send(outputJSON);
			}
		})

	} catch (e) {

	}
}

exports.getebayUpgradeFees = function(req, res) {
	ebayUpgradeFeesDB.find().exec(function(error, response) {
		if (error) {
			var outputJSON = {
				'status': 'failure',
				'messageId': 400,
				'message': 'Problem retrieving data',
				'err': error
			};
			return res.send(outputJSON);
		} else {

			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'Data Retrieved successfully',
				'data': response
			};
			return res.send(outputJSON);
		}
	})
}


exports.updateEbayUpgradeFee = function(req, res) {
	var body = req.body;
	console.log("body", body);
	var id = req.body._id;
	console.log("id>>", id);

	var filter = {
		_id: id
	}

	var updated = {
		"name": req.body.name,
		"under150": req.body.under150,
		"outer150": req.body.outer150
	}

	ebayUpgradeFeesDB.update(filter, updated).exec(function(error, response) {
		if (error) {
			var outputJSON = {
				'status': 'failure',
				'messageId': 400,
				'message': 'Problem retrieving data',
				'err': error
			};
			return res.send(outputJSON);
		} else {

			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'Data Retrieved successfully',
				'data': response
			};
			return res.send(outputJSON);
		}
	})
}

//END


//Function to implement reset Password
exports.adminResetPassword = function(req, res) {

	var details = req.body;
	var email = decrypt(req.body.email);

	var newPassword = req.body.password;
	var confirmPassword = req.body.confirm_password;
	var random_string = req.body.randomcode;
	if (newPassword == confirmPassword) {

		var filter = {
			"email": email,
			"randomstring": random_string
		}

		
    var newPass = jsmd5(newPassword);
		adminDB.update(filter, {
			$set: {
				"password": newPass
			}
		}, function(error, updateResults) {
			if (error) {
				var response = {
					"status": 'faliure',
					"messageId": 401,
					"message": "Error updating Password!!"
				};
				res.status(401).json(response);
			} else {
				console.log("updateResults", updateResults);
				var response = {
					"status": 'success',
					"messageId": 200,
					"message": "Password updated successfully!!"
				};
				res.status(200).json(response);
			}

		})
	} else {
		var response = {
			"status": 'failure',
			"messageId": 401,
			"message": "Password and confirm password does not match."
		};
		res.status(401).json(response);
	}


}


exports.exportFile = function(req, res) {
	try {

		sellerLoginDB.find({}, function(err, result) {

			var jsonArray = [];
			for (var i = 0; i < result.length; i++) {
				var data = {
					"Username": result[i].username,
					"Email": result[i].email,
					"Role": result[i].role,
					"Status": result[i].status
				}
				jsonArray.push(data);
			}

			res.xls('inventory.xlsx', jsonArray);
		})
	} catch (e) {

	}
}

exports.listSubscriptions = function(req, res, next = null) {
	try {
		if (req.body.flag == true) {
			next = null
		}
		subscriptionDB.find({
			isDeleted: false
		}).exec(function(err, response) {
			if (err) {
				var outputJSON = {
					'status': 'failure',
					'messageId': 400,
					'message': 'Problem retrieving data',
					'err': err
				};
				if (next == null) {
					return res.send(outputJSON);
				} else {
					next(outputJSON);
				}

			} else {

				var outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': 'Successfull Retrieval',
					'data': response
				};
				if (next == null) {

					return res.send(outputJSON);
				} else {

					next(outputJSON);
				}
			}
		})
	} catch (e) {

	}
}

exports.updatePlan = function(req, res) {
	var body = req.body;
	// console.log("body???", body.data._id);
	var filter = {
		_id: body.data._id
	}
	var updatedData = {
		plan_name: body.data.plan_name,
		subscription_price: parseFloat(body.data.subscription_price),
		order_limit: parseFloat(body.data.order_limit),
		maximum_selling_channel: parseFloat(body.data.maximum_selling_channel)
	}

	subscriptionDB.update(filter, updatedData, {
		upsert: true
	}).exec(function(err, response) {
		if (err) {
			var outputJSON = {
				'status': 'failure',
				'messageId': 400,
				'message': 'Problem updating data',
				'err': err
			};
			return res.send(outputJSON);
		} else {
			console.log("response>>>", response);
			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'Successfully Updated',
				'data': response
			};
			return res.send(outputJSON);
		}
	})

}



exports.deleteUser = function(req, res) {
	var body = req.body;
	var filter = {
		_id: req.body.id
	}
	var updatedData = {
		isDeleted: true
	}
	sellerLoginDB.update(filter, updatedData).exec(function(err, response) {
		if (err) {
			var outputJSON = {
				'status': 'failure',
				'messageId': 400,
				'message': 'Problem updating data',
				'err': err
			};
			return res.send(outputJSON);
		} else {
			console.log("response>>>", response);
			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'Successfully Updated',
				'data': response
			};
			return res.send(outputJSON);
		}
	})
}


//By dixit
exports.userDetails = function(req, res) {
	console.log("User List Function ");
	var id = req.session.admin.user._id;

	adminDB.findOne({
		"_id": id
	}, function(err, result) {
		if (err) {
			var outputJSON = {
				'status': 'failure',
				'messageId': 400,
				'message': 'Problem updating data',
				'err': err
			};
			return res.send(outputJSON);
		} else {
			console.log("response>>>", result);
			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'Successfully Get',
				'data': result
			};
			return res.send(outputJSON);
		}
	});
}



//END
exports.changeUserStatus = function(req, res) {
	var body = req.body;
	var userStatus = body.status;
	console.log("The User Current Status is : ", userStatus);
	var filter = {
		_id: req.body._id
	}
	var updatedData = {};

	if (userStatus == 'active') {
		updatedData = {
			status: "deactive"
		}
	} else {
		updatedData = {
			status: "active"
		}
	}
	console.log("The User Updatre Data is : ", updatedData);


	sellerLoginDB.update(filter, updatedData).exec(function(err, response) {
		if (err) {
			var outputJSON = {
				'status': 'failure',
				'messageId': 400,
				'message': 'Problem updating data',
				'err': err
			};
			return res.send(outputJSON);
		} else {
			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'Successfully Updated',
				'data': response
			};
			return res.send(outputJSON);
		}
	})
}


exports.updateAdmin = function(req, res) {
	var profileData = req.body;
	console.log("profileData", profileData);
	if (req.body.password == "yes") {
		console.log("herrrrrrrrrrrrrrrrrrrrrrrrrreeee");
		var updatedProfile = {
			username: profileData.username,
			first_name: profileData.first_name,
			last_name: profileData.last_name,
			password: jsmd5(profileData.new_password)
		}
	} else {
		// profileData.current_password = req.session.admin.user.password;
		var updatedProfile = {
			username: profileData.username,
			first_name: profileData.first_name,
			last_name: profileData.last_name
		}
	}

	var filter = {
		_id: profileData._id,
		// password: profileData.current_password,
		isDeleted: false
	}

	console.log("updatedProfile", updatedProfile);
	adminDB.findOneAndUpdate(filter, updatedProfile, {
		new: true
	}, function(error, updatedResults) {
		if (error) {
			var response = {
				"status": 'Failure',
				"messageId": 400,
				"message": "Error updating Profile!!"
			};
			res.status(400).json(response);
		} else {

			console.log("updatedResults----------", updatedResults);
			//console.log("session",req.session.user);
			if (updatedResults) {
				// req.session.user.user.username = updatedResults.username;
				// req.session.user.user.first_name = updatedResults.first_name;
				// req.session.user.user.last_name = updatedResults.last_name;
				req.session.admin.user.username = updatedResults.username;
				req.session.admin.user.first_name = updatedResults.first_name;
				req.session.admin.user.last_name = updatedResults.last_name;
				req.session.admin.user.password = updatedResults.password;
				console.log("session now", req.session.admin);
				var response = {
					"status": 'Success',
					"messageId": 200,
					"message": "Profile updated successfully!!",
					"data": updatedResults
				};
			} else {
				var response = {
					"status": 'Failure',
					"messageId": 400,
					"message": "Your current password is wrong!"
				};
			}
			res.status(200).json(response);

		}
	})
}



exports.adminInformation = function(req, res) {
	console.log("Admin INformation Pannel");
	var id = req.session.admin.admin._id;
	adminDB.findOne({
		"_id": id
	}, function(err, response) {
		if (err) {
			var outputJSON = {
				'status': 'failure',
				'messageId': 400,
				'message': 'Problem updating data',
				'err': err
			};
			return res.send(outputJSON);
		} else {
			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'Successfully Updated',
				'data': response
			};
			return res.send(outputJSON);
		}
	});
}

exports.addNewAdmin = function(req, res) {
	var body = req.body;
	var newAdmin = new adminDB({
		"username": body.username,
		"email": body.email,
		"first_name": body.first_name,
		"last_name": body.last_name,
		"password": body.password
	})
	console.log("The NewADmin is : ", newAdmin);
	newAdmin.save(function(err, response) {
		if (err) {
			var outputJSON = {
				'status': 'failure',
				'messageId': 400,
				'message': 'Problem Creating Admin',
				'err': err
			};
			return res.send(outputJSON);
		} else {
			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'Successfully Created Admin',
				'data': response
			};
			return res.send(outputJSON);
		}
	});
}


exports.adminList = function(req, res) {

		try {
			var page = req.body.page || 1,
				count = req.body.count || 1;
			console.log("req", req.body);
			var searchQry = req.body.search;


			var sortkey = null;
			for (key in req.body.sort) {
				console.log("The keys is : ", key);
				sortkey = key;
			}
			if (sortkey) {
				var sortquery = {};
				sortquery[sortkey ? sortkey : 'createdOn'] = req.body.sort ? (req.body.sort[sortkey] == 'desc' ? -1 : 1) : -1;
			}

			var filter = {};
			filter.$and = [{
				"isDeleted": false,
				"designation": "Admin"
			}];

			if (req.body.search) {
				filter.$or = [{
					username: {
						$regex: searchQry,
						$options: 'i'
					}
				}, {
					email: {
						$regex: searchQry,
						$options: 'i'
					}
				}]

			}
			console.log("filter", JSON.stringify(filter));
			var skipNo = (page - 1) * count;

			adminDB.find().and(filter).sort(sortquery).skip(skipNo).limit(count).exec(function(error, response) {
				if (error) {
					console.log("The Error is : ", error);
					var outputJSON = {
						'status': 'failure',
						'messageId': 400,
						'message': 'Problem Admin List',
						'err': error
					};
					return res.send(outputJSON);
				} else {
					console.log("response", response);
					adminDB.find().and(filter).count().exec(function(err, total) {
						console.log("Count : ", total);
						var outputJSON = {
							'status': 'success',
							'messageId': 200,
							'message': 'data retrieve from products',
							'data': response,
							'count': total
						}
						return res.send(outputJSON);
					})
				}
			})
		} catch (e) {
			console.log("exception", e);
		}
	}
	/*   End Of User List Function */

exports.deletesubAdmin = function(req, res) {
	var id = req.body._id;
	var filter = {
		"_id": id
	}
	var set = {
		isDeleted: true
	}
	adminDB.update(filter, set).exec(function(err, response) {
		if (err) {
			console.log("The Error is : ", error);
			var outputJSON = {
				'status': 'failure',
				'messageId': 400,
				'message': 'Problem Deleting Sub-Admin.',
				'err': error
			};
			return res.send(outputJSON);
		} else {


			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'Sub-Admin deleted successfully.',
				'data': response,
				
			}
			return res.send(outputJSON);

		}
	})
}


/****************Dashboard function ***************/
exports.userList = function(req,res){
	sellerLoginDB.find({"isDeleted" :false}).count().exec(function(err,done){
		if(err){

		}
		else{
				console.log("*-******-",done)
		}
	})
}




//END