var usersDB = require("./../model/usersModel.js");
var subsriptions = require('./../model/subscriptionModel.js');
exports.getusers = function(req, res) {
    try {
      console.log('in user controller  ', req.body)
        var page = req.body.page || 1,
            count = req.body.count || 1;
        var skipNo = (page - 1) * count;
        var searchQry = req.body.search;
        var sortkey = null;
        for (key in req.body.sort) {
          sortkey = key;
        }
        if (sortkey) {
          var sortquery = {};
          sortquery[sortkey ? sortkey : 'createdOn'] = req.body.sort ? (req.body.sort[sortkey] == 'desc' ? -1 : 1) : -1;
        }
        var filter = [{"isDeleted": false}];
        if(req.body.search){
            filter.push({
              $or:
              [
              { email: {
                  $regex : searchQry,
                  $options: 'i'
                }
              },
              { fullName: {
                  $regex : searchQry,
                  $options: 'i'
                }
              },
              { phoneNumber: {
                  $regex : searchQry,
                  $options: 'i'
                }
              }
              ]
            });
      }
        usersDB.find().populate('language').and(filter).sort(sortquery).skip(skipNo).limit(count).exec(function(err, result) {
            if (err) {

                var outputJSON = {
                    'status': 'failure',
                    'messageId': 203,
                    'message': 'data not retrieved'
                };
                return res.send(outputJSON);
            } else {
                usersDB.find().and(filter).count().exec(function(err, total) {
                    var outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': 'data retrieve Successfully',
                        'data': result,
                        'count': total
                    }
                    return res.send(outputJSON);
                })
            }
        })
    } catch (e) {

    }
}

exports.getSubscriptions = function(req, res) {
    try {
      console.log('in getSubscriptions controller  ', req.body)
        var page = req.body.page || 1,
            count = req.body.count || 1;
        var skipNo = (page - 1) * count;
        var searchQry = req.body.search;
        var sortkey = null;
        for (key in req.body.sort) {
          sortkey = key;
        }
        if (sortkey) {
          var sortquery = {};
          sortquery[sortkey ? sortkey : 'listed_date'] = req.body.sort ? (req.body.sort[sortkey] == 'desc' ? -1 : 1) : -1;
        }
        var filter = {};
        if(req.body.search){
            filter.$or = [
              { "user_id.email": {
                  $regex : searchQry,
                  $options: 'i'
                }
              },
              { "user_id.fullName": {
                  $regex : searchQry,
                  $options: 'i'
                }
              },
              { "user_id.phoneNumber": {
                  $regex : searchQry,
                  $options: 'i'
                }
              }
              ]
      }
        subsriptions.find().populate('user_id').and(filter).sort(sortquery).skip(skipNo).limit(count).exec(function(err, result) {
            if (err) {

                var outputJSON = {
                    'status': 'failure',
                    'messageId': 203,
                    'message': 'data not retrieved'
                };
                return res.send(outputJSON);
            } else {
                subsriptions.find().and(filter).count().exec(function(err, total) {
                    var outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': 'data retrieve Successfully',
                        'data': result,
                        'count': total
                    }
                    return res.send(outputJSON);
                })
            }
        })
    } catch (e) {

    }
}


exports.addSupplier = function(req, res) {
    console.log("In add Supplier");
    try {
        var body = req.body;
        if (!body.name) {
            return res.status(203).send({
                'status': 'failure',
                'messageId': 203,
                'message': 'Name not given'
            });
        }
        if (!body.email) {
            return res.status(203).send({
                'status': 'failure',
                'messageId': 203,
                'message': 'Email not given'
            });
        }
        if (!body.phone) {
            return res.status(203).send({
                'status': 'failure',
                'messageId': 203,
                'message': 'Phone Number not given'
            });
        }
        if (!body.attn_to) {
            return res.status(203).send({
                'status': 'failure',
                'messageId': 203,
                'message': 'Attn to not given'
            });
        }
        if (!body.zip_code) {
            return res.status(203).send({
                'status': 'failure',
                'messageId': 203,
                'message': 'Zip code not given'
            });
        }
        if (!body.city) {
            return res.status(203).send({
                'status': 'failure',
                'messageId': 203,
                'message': 'City not given'
            });
        }
        if (!body.state) {
            return res.status(203).send({
                'status': 'failure',
                'messageId': 203,
                'message': 'State not given'
            });
        }
        if (!body.country) {
            return res.status(203).send({
                'status': 'failure',
                'messageId': 203,
                'message': 'Country Name not given'
            });
        }
        supplierDB.create(body, function(err, result) {
            if (err) {
                return res.status(203).send({
                    'status': 'failure',
                    'messageId': 203,
                    'message': "There is some error"
                });
            } else {
                var outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': 'Supplier Added Successfully',
                    'data': result
                }
                return res.send(outputJSON);
            }
        })
    } catch (e) {

    }
}

exports.updateUser = function(req, res) {
    try {
        var body = req.body;
        if (!body._id) {
            return res.status(203).send({
                'status': 'failure',
                'messageId': 203,
                'message': 'Id not found'
            });
        }
        var update = {};
        if(req.body.newstatusval == false || req.body.newstatusval == true){
            update["status"] = req.body.newstatusval;
        }else {
            update["isDeleted"] = true;
        }

        update["updatedOn"] = new Date();
        var filter = { "_id": body._id }
        usersDB.findOneAndUpdate(filter, { $set: update }, { new: true }, function(err, result) {
            if (err) {
                return res.status(203).send({
                    'status': 'failure',
                    'messageId': 203,
                    'message': 'There is some Problem to delete record'
                });
            } else {
                var outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': 'Record updated Successfully.',
                    'data': result
                }
                return res.send(outputJSON);
            }
        })
    } catch (e) {

    }
}


exports.uploadFile = function(req, res) {
    try {
        if (!req.files)
            return res.status(400).send('No files were uploaded.');
        var sampleFile = req.files.file;
        var array = req.files.file.name.split('.');
        var fileName = Date.now() + '.' + array[array.length - 1];
        sampleFile.mv("./uploads/" + fileName, function(err) {
            if (err) {
                return res.status(500).send(err);
            } else {
                return res.send(fileName);
            }
        });
    } catch (e) {
        
    }
}
