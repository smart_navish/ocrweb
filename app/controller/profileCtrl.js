var sellerLoginDB = require("./../model/sellerModel.js");
var jsmd5 = require('js-md5');

exports.updateProfile = function(req, res) {
	var profileData = req.body;
	console.log("profileData",profileData);
	if (req.body.password == "yes") {
		var updatedProfile = {
			username: profileData.username,
			first_name: profileData.firstname,
			last_name: profileData.lastname,
			password:jsmd5(profileData.new_password)
		}
	} else {
		console.log("no password");
		var updatedProfile = {
			username: profileData.username,
			first_name: profileData.firstname,
			last_name: profileData.lastname
		}
	}

	var filter = {
		_id: profileData._id
	}

    console.log("filter",filter);
	sellerLoginDB.findOneAndUpdate(filter, updatedProfile,{new: true} ,function(error, updatedResults) {
		console.log("updatedResults",updatedResults);
		if (error) {
			var response = {
				"status": 'Failure',
				"messageId": 400,
				"message": "Error updating Profile!!"
			};
			res.status(400).json(response);
		} else {


			if(updatedResults){
				req.session.user.user.username = updatedResults.username;
				req.session.user.user.first_name = updatedResults.first_name;
				req.session.user.user.last_name = updatedResults.last_name;
				req.session.user.user.password = updatedResults.password;
				console.log("session now",req.session.user);
				var response = {
					"status": 'Success',
					"messageId": 200,
					"message": "Profile updated successfully!!",
					"data": updatedResults
				};
			}
			else {
				var response = {
					"status": 'Failure',
					"messageId": 400,
					"message": "Error while updating!!"
				};
			}
			res.status(200).json(response);

		}
	})
}
