app.controller('peController', function($scope, $http, authenticationService, $uibModal, $document, $log, $window, $location, $state) {

    $scope.clicked = false;
    $scope.shopifyClicked = false;
$scope.checkState = function(state) {
		if ($state.current.name == state) {
			return true;
		} else {
			return false;
		}
	}

	$scope.status = {
		isopen: false
	};

	$scope.toggled = function(open) {
		$log.log('Dropdown is now: ', open);
	};

	$scope.toggleDropdown = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.status.isopen = !$scope.status.isopen;
	};

	/* slim scrollbar */

	var self = this;

	/* left sidebar open and right sidebar open and close */
	$scope.leftside_close = false;
	$scope.leftbar_close = function() {
		$scope.leftside_close = !$scope.leftside_close;
	}

	$scope.rightside_open = false;
	$scope.rightbar_open = function() {
			$scope.rightside_open = !$scope.rightside_open;
	}
		/* invenotry list show */

	$http({
		method: "GET",
		url: 'json/inventory.json',
		contentType: "application/json; ",
		dataType: "json",

	}).then(function(response) {
		$scope.inventory_data = response.data.inventory;
	});

	$scope.removeItem_inventory = function(item) {
		var index = $scope.inventory_data.indexOf(item);
		$scope.inventory_data.splice(index, 1);
	}

	$scope.$on('wareHouses', function(event, args) {
		$scope.warehouseData = {};
		$scope.warehouseData.warehouseOne = args["wareHouseQty"];
		$scope.warehouseData.amazonFBAWareHouseQty = args["amazonFBAWareHouseQty"];

		console.log("$scope.warehouseData", $scope.warehouseData);
	});


	$scope.$on('OpenDataInModal', function(a, b) {
		$scope.showDetails = b;

	})

	$scope.$on('ViewEbayData', function(event, data) {
		$scope.ItemURL = data.ListingDetails.ViewItemURL;
		$scope.QuantityAvailable = data.QuantityAvailable;
		if (typeof(data.SellerProfiles) == 'object') {
			$scope.PaymentProfile = data.SellerProfiles.SellerPaymentProfile.PaymentProfileName;
		}
	})


	$scope.$on('showPurchaseOrderDetails', function(name, value) {
        $scope.name = value.supplier_id.name;
		$scope.check = value.check;
		$scope.ship_to = value.shipping_address.split('\n');
		$scope.ship_via = value.ship_via;
		$scope.po_date = value.po_date;
		$scope.shipping_custom_charges = value.shipping_custom_charges;
		$scope.purchaseOrderArray = value.purchaseOrderDetails;

	})

	$scope.confirmOrder = function(data, check, notes) {
		$scope.$broadcast('Confirm', data, check, notes);
	}


	$scope.warehouse_id = {};
	$scope.warehouse_tag = {};
	$scope.previous_qty = [];
  $scope.popupval = [];
	$scope.$on('showActualandRecieve', function(event, data) {
		$scope.poObj = {
			"po_id": data._id,
			"order_status": data.order_status,
			"amount_paid": parseFloat(data.amount_paid),
			"total_PO": data.total_PO
		};
		var array = data.purchaseOrderDetails;
		array.forEach(function(value, key) {
			$scope.previous_qty[key] = value.received_quantity;
			console.log("$scope.previous_qty", $scope.previous_qty[key]);
      $scope.popupval[key] = false;
		})
		$scope.logArray = data.orderLogArray;
		$scope.warehouse_name = data.warehouse_name;
		$scope.warehouse_id = data.warehouseid;
		$scope.warehouse_tag = data.warehouse_tag;
		$scope.purchaseOrderDetails = data.purchaseOrderDetails;
	});

	$scope.updateQuantity = function(data) {
    console.log('data---------------->>>>>>>>>> ', data);
		// console.log("data123", data);
		data.forEach(function(value, key) {
				// console.log("value", value, "key", key);
				value.warehouseid = $scope.warehouse_id;
				value.warehouse_tag = $scope.warehouse_tag;
				value.previous_qty = $scope.previous_qty[key];
    })
		$scope.$broadcast('updateGoodsQuantity', data);
	}

	$scope.closePO = function(data) {
        $scope.$broadcast('closePO', data);
	}

	$scope.logout = function() {
		authenticationService.logout().then(function(response) {
			if (response) {
				var protocol = $location.protocol();
				var host = protocol + '://' + $location.host();
				var port = $location.port();
				var sublink = "login";
				var FullLink = host + ':' + port + '/';
				$window.location.href = FullLink;
            }
		}, function(err) {
			console.log("err", err);
		});
	}

	$scope.addShopify = function() {
		$scope.$broadcast('shopifyValue', $scope.Shopify)
	}
	$scope.addAmazon = function() {
		$scope.$broadcast('amazonValue', $scope.Amazon)
	}

	$scope.$on('viewDetailsInfo', function(event, data, channel) {
		$scope.salesChannel = data;
		$scope.channel = channel;
	});

	$scope.$on('mapInventory', function(event, data) {
		$scope.datatoview = data;
		$scope.seller_info = data.seller_info;
		$scope.skusModel = {};
	});

	$scope.$on('mapInventoryEbay', function(event, data) {
		$scope.ebayData = data;
		$scope.ebayFees = data.ebayfess_info.fees;
		$scope.skusModel = {};
	});

	$scope.estimatedShopifyVarMargin = {};
	$scope.dbreakDown = {};
	$scope.netProfitestimate = {};
	$scope.dbreakDownnet = {};
	$scope.shopifystockleft = {};
	var rootbreakDownValues = {};
	$scope.shopifyMultiVariantCalculation = function(data, shopifyPlan, index) {
		var mappingArray = data.mappingValues;
		$scope.variantTotal = 0;
		mappingArray.forEach(function(value, key) {
			if (value.inventory_info.length) {
				$scope.variantTotal = $scope.variantTotal + (value.quantity * value.inventory_info[0].avgUnitCost);
				if(key == 0){
						$scope.shopifystockleft[index] = (value.inventory_info[0].qty) / (value.quantity)
				}else {
						if($scope.shopifystockleft[index] > (value.inventory_info[0].qty) / (value.quantity)){
								$scope.shopifystockleft[index] = (value.inventory_info[0].qty) / (value.quantity);
						}
				}

			}
			$scope.shopifystockleft[index] = Math.floor($scope.shopifystockleft[index]);
			if ($scope.variantTotal) {
				$scope.estimatedShopifyVarMargin[index] = (((data.price - ((data.price * shopifyPlan / 100) + (0.3) + $scope.variantTotal + data.costToShip)) / data.price) * (100))
				$scope.estimatedShopifyVarMargin[index] = $scope.estimatedShopifyVarMargin[index].toFixed(2);
				$scope.netProfitestimate[index] = (data.price - ((data.price * shopifyPlan / 100) + (0.3) + $scope.variantTotal + data.costToShip));
				rootbreakDownValues[index] = `<table class="table table-bordered" border="1">
							<tr>
								<td>
									<strong>Price : <strong>
								</td>
								<td>
									<strong>`+data.price+`<strong>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Shopify Fee: <strong>
								</td>
								<td>
									<strong>`+((data.price * (shopifyPlan) / 100)+ (0.3)).toFixed(2)+`<strong>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Cost To Acquire : <strong>
								</td>
								<td>
									<strong>`+$scope.variantTotal.toFixed(2)+`<strong>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Cost To Ship : <strong>
								</td>
								<td>
									<strong>`+data.costToShip.toFixed(2)+`<strong>
								</td>
							</tr>
				</table>`;

				$scope.dbreakDown[index] = rootbreakDownValues[index] + '<h4><small> [(Price - (Ebay Fee + Paypal Fee + Cost to Acquire + Cost To Ship)) / Price] * 100</small></h4>'
				$scope.dbreakDownnet[index] = rootbreakDownValues[index] + '<h4><small> [(Price - (Ebay Fee + Paypal Fee + Cost to Acquire + Cost To Ship))]</small></h4>'
				//$scope.dbreakDown[index] =  "[("+data.price +"- ("+((data.price * (shopifyPlan) / 100)+ (0.3)).toFixed(2) +"+"+ $scope.variantTotal.toFixed(2) +"+"+data.costToShip+")) / "+data.price+"] * 100";
				//$scope.dbreakDownnet[index] = "[("+data.price +"- ("+((data.price * (shopifyPlan) / 100)+ (0.3)).toFixed(2) +"+"+ $scope.variantTotal.toFixed(2) +"+"+data.costToShip+"))]";

			} else {
				$scope.estimatedShopifyVarMargin[index] = 0;
				$scope.netProfitestimate[index] = 0;
				$scope.dbreakDown[index] = "No break down available";
				$scope.dbreakDownnet[index] = "No break down available";
				$scope.shopifystockleft[index] = 0;
			}
		})
	}

  $scope.estimatedeBayVarMargin = {};
	$scope.estimatedEbayNetdata = {};
	$scope.ebaybreakDowndata = {};
	$scope.ebaynetbreakDowndata = {};
	$scope.ebaystockleftdata = {};
	var ebayrootbreakDownValues = {};
	$scope.eBayMultiVariantCalculation = function(data,index) {
		var mappingArray = data.mappingValues;
		$scope.eBayVariantTotal = 0;
		mappingArray.forEach(function(value, key) {
			if (value.inventory_info.length) {
				$scope.eBayVariantTotal = $scope.eBayVariantTotal + (value.quantity * value.inventory_info[0].avgUnitCost);
				if(key == 0){
						$scope.ebaystockleftdata[index] = (value.inventory_info[0].qty) / (value.quantity)
				}else {
						if($scope.ebaystockleftdata[index] > (value.inventory_info[0].qty) / (value.quantity)){
								$scope.ebaystockleftdata[index] = (value.inventory_info[0].qty) / (value.quantity);
						}
				}
			}
			$scope.ebaystockleftdata[index] = Math.floor($scope.ebaystockleftdata[index]);
			if ($scope.eBayVariantTotal) {
				$scope.estimatedeBayVarMargin[index] =  (((data.StartPrice - ((data.StartPrice * $scope.ebayFees /100) + (data.StartPrice * 2.9/100) + (0.3) + ($scope.eBayVariantTotal) + (data.costToShip))) / data.StartPrice) * (100))
				$scope.estimatedeBayVarMargin[index] = $scope.estimatedeBayVarMargin[index].toFixed(2);
				ebayrootbreakDownValues[index] = `<table class="table table-bordered" border="1">
							<tr>
								<td>
									<strong>Price : <strong>
								</td>
								<td>
									<strong>`+data.StartPrice.toFixed(2)+`<strong>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Ebay Fee: <strong>
								</td>
								<td>
									<strong>`+(data.StartPrice * $scope.ebayFees /100).toFixed(2)+`<strong>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Paypal Fee : <strong>
								</td>
								<td>
									<strong>`+(data.StartPrice * 2.9/100).toFixed(2)+`<strong>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Cost To Acquire : <strong>
								</td>
								<td>
									<strong>`+$scope.eBayVariantTotal.toFixed(2)+`<strong>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Cost To Ship : <strong>
								</td>
								<td>
									<strong>`+data.costToShip.toFixed(2)+`<strong>
								</td>
							</tr>
				</table>`;
				$scope.ebaybreakDowndata[index] = ebayrootbreakDownValues[index] + '<h4><small> [(Price - (Ebay Fee + Paypal Fee + Cost to Acquire + Cost To Ship)) / Price] * 100</small></h4>'
				$scope.ebaynetbreakDowndata[index] = ebayrootbreakDownValues[index] + '<h4><small> [(Price - (Ebay Fee + Paypal Fee + Cost to Acquire + Cost To Ship))]</small></h4>'
				$scope.estimatedEbayNetdata[index] = (data.StartPrice - ((data.StartPrice * $scope.ebayFees /100) + (data.StartPrice * 2.9/100) + (0.3) + $scope.eBayVariantTotal + data.costToShip));
				// $scope.ebaybreakDowndata[index] =  "[("+data.StartPrice +"- ("+((data.StartPrice * $scope.ebayFees /100) + (data.StartPrice * 2.9/100) + (0.3)).toFixed(2) +"+"+ $scope.eBayVariantTotal.toFixed(2) +"+"+data.costToShip+")) / "+data.StartPrice+"] * 100";
				// $scope.ebaynetbreakDowndata[index] = "[("+data.StartPrice +"- ("+((data.StartPrice * $scope.ebayFees /100) + (data.StartPrice * 2.9/100) + (0.3)).toFixed(2) +"+"+ $scope.eBayVariantTotal.toFixed(2) +"+"+data.costToShip+"))]";
			} else {
				$scope.estimatedeBayVarMargin[index] = 0;
				$scope.estimatedEbayNetdata[index] = 0;
				$scope.ebaybreakDowndata[index] = "No break down available";
				$scope.ebaynetbreakDowndata[index] =  "No break down available";
				$scope.ebaystockleftdata[index] = 0;
			}
		})
	}

	$scope.skuMapInventory = function(data, operation, id = null) {
		var addArray = {
			"sku_value": "",
			"quantity": "",
			"inventory_id": ""
		};
		switch (operation) {
			case 'addNew':
				$scope.skusModel = {
					Id: id,
					variantsId: data.id,
					costToShip: data.costToShip ? data.costToShip : '0.00',
					skusArray: (data.mappingValues && data.mappingValues.length > 0) ? data.mappingValues : []
				}
				if (!data.mappingValues) {
					$scope.skusModel.skusArray.push(addArray);
				}
				$scope.shopifyClicked = true;
				break;
			case 'addMore':
				$scope.skusModel.skusArray.push(addArray);
				$scope.shopifyClicked = true;
				break;
			case 'removeNew':
				$scope.skusModel.skusArray.splice(data, 1);
				$scope.shopifyClicked[data] = false;
				break;
			case 'saveValues':
                if ($scope.skusModel.skusArray.length > 0) {
					$scope.$broadcast('saveShopifyMappingValues', $scope.skusModel);
				}
			default:
		}
	}



	$scope.eBayskuMapInventory = function(data, operation, id) {
    var addArray = {
			"sku_value": "",
			"quantity": "",
			"inventory_id": ""
		};
		switch (operation) {
			case 'addNew':
				$scope.skusModel = {
					Id: data._id,
					ebayInventoryId: id,
					costToShip: data.costToShip ? data.costToShip : '0.00',
					skusArray: (data.mappingValues && data.mappingValues.length > 0) ? data.mappingValues : []
				}
				if (!data.mappingValues) {
					$scope.skusModel.skusArray.push(addArray);
				}

			    $scope.clicked = true;

				break;
			case 'addMore':
				$scope.skusModel.skusArray.push(addArray);
				$scope.clicked = true;
				break;
			case 'removeNew':
				$scope.skusModel.skusArray.splice(data, 1);

				 $scope.clicked[data] = false

				break;
			case 'saveValues':
				if ($scope.skusModel.skusArray.length > 0) {
					$scope.$broadcast('saveeBayMappingValues', $scope.skusModel);
				}
			default:
		}
	}



	$scope.selectedItem = function(selected) {
		if (selected) {
			$scope.skusModel.skusArray[this.id].sku_value = selected.title;
			$scope.skusModel.skusArray[this.id].inventory_id = selected.originalObject._id;
		}
	}

	$scope.contactAmazon = function() {
		$window.open('https://sellercentral.amazon.com/gp/mws/registration/register.html', '_blank');
	}

	$scope.remoteUrlRequestFn = function(str) {
		return {
			search: str
		};
	}

	$scope.savePayPalEmail = function(data) {
		$scope.$broadcast('savePayPalEmail', data);
	}

	$scope.$on('priceConfirmationList', function(event, data) {
		$scope.cnfmData = data;
		$scope.tempcnfmData = angular.copy($scope.cnfmData);
	})

	$scope.saveCnfmData = function(data) {
		$scope.$broadcast('shopifysaveCnfmData', data);
	}

	$scope.$on('viewEbayOrders', function(event, args) {
		$scope.shippingAddress = args.mainData.shippingAddress;
    });

	$scope.saveEbayCnfmData = function(data) {
		$scope.$broadcast('ebaysaveCnfmData', data);
	}

	$scope.$on('checkPlan', function(event, planName, price) {
		$scope.planName = planName;
		$scope.price = price;
	})

	$scope.setPaymentExpress = function(planName, price, payments) {
		$scope.payments = {};
		$scope.payments.planName = planName;
		$scope.payments.price = price;
		$scope.payments.LANDINGPAGE = payments;
		$scope.$broadcast('setPaymentExpressinfo', $scope.payments);
	}

	$scope.$on('itemOrdered', function(event, data) {
		console.log("data",data)
		if (data.label == 'Shopify') {
			$scope.itemArray = [];
			$scope.line_items = data.shopifyItemInfo;
			$scope.shopifyImages = data.shopifyImages;
		}
    else if (data.label == 'Amazon') {
			$scope.itemArray = [];
			$scope.line_items = data.shopifyItemInfo;
			$scope.shopifyImages = data.shopifyImages;
		}
    else {
			$scope.line_items = [];
			$scope.itemArray = data.itemInfo;
			$scope.imageArray = data.imageArray;
			}
    });

    $scope.$on('showAddress',function(event,data){
        console.log("data in main controller",data);
        $scope.shippingAddress = data.shippingAddress;
    })

    $scope.getImgResult = {};
    $scope.getImageController = function(itemID, indexVal)
    {
        $scope.getImgResult[indexVal] = $scope.imageArray.filter(function(obj){
    		return obj.ItemID == itemID;
    	})

    }


    $scope.getShopifyImg = {};
    $scope.getShopifyImage = function(itemID, indexVal)
    {
        $scope.getShopifyImg[indexVal] = $scope.shopifyImages.filter(function(obj){
    		return obj.product_id == itemID;
    	})

    }




	$scope.shopifyPlan = [{price : 2.9, options : "Basic_Shopify"},
    {price : 2.6, options : "Shopify"},
    {price : 2.4, options : "Advanced_Shopify"}];
	$scope.shippingPolicy = [];
	for(var i = 1; i <= 7; i++){
			var optionsData = i+" Business Day";
			$scope.shippingPolicy.push(optionsData)
	}
	$scope.PaymentPolicy = ['CC & Paypal', 'Paypal'];

	//roundOfValue value function
	$scope.roundof = 0;


	$scope.roundOfValue = function() {
		if($scope.roundof == 0){
			if($scope.tempcnfmData.length > 0){
				$scope.tempcnfmData.forEach(function(vals, keys){
						$scope.cnfmData[keys].newPrice = vals.newPrice;
				});
			}
		}else {
			if($scope.tempcnfmData.length > 0){
				$scope.tempcnfmData.forEach(function(vals, keys){
						$scope.cnfmData[keys].newPrice = Math.round10(vals.newPrice, -1);
				});
			}
		}

	}


	// Closure

  var decimalAdjust = function(type, value, exp) {
    // If the exp is undefined or zero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // If the value is negative...
    if (value < 0) {
      return -decimalAdjust(type, -value, exp);
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  // Decimal round
  if (!Math.round10) {
    Math.round10 = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  // Decimal floor
  if (!Math.floor10) {
    Math.floor10 = function(value, exp) {
      return decimalAdjust('floor', value, exp);
    };
  }
  // Decimal ceil
  if (!Math.ceil10) {
    Math.ceil10 = function(value, exp) {
      return decimalAdjust('ceil', value, exp);
    };
  }



  //Date Picker

  $scope.today = function() {
    $scope.start_date = new Date();
    $scope.valid_date = new Date();
  };
  $scope.today();

  $scope.clear = function() {
    $scope.start_date = null;
    $scope.valid_date = null;
  };

  // $scope.inlineOptions = {
  //   customClass: getDayClass,
  //   minDate: new Date(),
  //   showWeeks: true
  // };

  $scope.dateOptions = {

    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(1980, 5, 22),
    startingDay: 1,
    format: 'dd/mm/yyyy'
  };

  $scope.open1 = function(indexVal) {
    $scope.popupval[indexVal] = true;
  };
  $scope.openFrom = function() {
    $scope.popupvalFrom.opened = true;
  };
  $scope.openTo = function() {
    $scope.popupvalTo.opened = true;
  };

  $scope.popupvalFrom = {
    opened: false
  };

  $scope.popupvalTo = {
    opened: false
  };





  $scope.setDate = function(year, month, day) {
    $scope.start_date = new Date(day, month, year);
    $scope.valid_date = new Date(day, month, year);
  };

  $scope.formats = ['dd-MMMM-yyyy'];
  $scope.format = $scope.formats[0];
  $scope.altInputFormats = ['M!/d!/yyyy'];



  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $scope.events = [{
    date: tomorrow,
    status: 'full'
  }, {
    date: afterTomorrow,
    status: 'partially'
  }];

  // function getDayClass(data) {
  //   var date = data.date,
  //     mode = data.mode;
  //   if (mode === 'day') {
  //     var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
  //     for (var i = 0; i < $scope.events.length; i++) {
  //       var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);
  //       if (dayToCheck === currentDay) {
  //         return $scope.events[i].status;
  //       }
  //     }
  //   }
  //   return '';
  // }
  //END

});
