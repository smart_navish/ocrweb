var app = angular.module('pricingeasy', ['textAngular', 'ui.router', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'jkuri.slimscroll', 'ngTable', 'toastr', 'ngDialog', 'angularFileUpload', 'angucomplete-alt', 'chart.js', 'naif.base64','base64']);

/* Routing */

// app.config(['$locationProvider', function($locationProvider) {
//   //$locationProvider.hashPrefix('');
// }]);

app.config(function($stateProvider, $urlRouterProvider) {
  var authenticate = function($q, $http, $window, $location, toastr) {
    var deferred = $q.defer();
    // console.log("deferred", deferred);
    $http.get('/login/checkSession').then(function(response) {
      if (response.data.messageId == 200) {
        deferred.resolve("Success");
      } else {
        deferred.reject();

        var protocol = $location.protocol();
        var host = protocol + '://' + $location.host();
        var port = $location.port();
        var sublink = "login";
        // var FullLink = host + ':' + port + '/' + sublink;
        var FullLink = host + ':' + port + '/';
        $window.location.href = FullLink;
      }
      return deferred.promise;
    })
  };

  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('dashboard', {
      url: '/',
      views: {
        "": {
          templateUrl: 'modules/dashboard/view/dashboard.html',
          controller: 'dashboardctrl',
          resolve: {
            auth: authenticate
          }
        },
        "header": {
          templateUrl: 'includes/header.html'
        },
        "sidebar": {
          templateUrl: 'includes/sidebar.html'
        }
      }
    })

  .state('users', {
      url: '/users',
      views: {
        "": {
          templateUrl: 'modules/users/view/users.html',
          controller: 'usersCtrl',
          resolve: {
            auth: authenticate
          }
        },
        "header": {
          templateUrl: 'includes/header.html'
        },
        "sidebar": {
          templateUrl: 'includes/sidebar.html'
        }
      }

    })

    .state('subscriptions', {
        url: '/subscriptions',
        views: {
          "": {
            templateUrl: 'modules/users/view/subscription.html',
            controller: 'usersCtrl',
            resolve: {
              auth: authenticate
            }
          },
          "header": {
            templateUrl: 'includes/header.html'
          },
          "sidebar": {
            templateUrl: 'includes/sidebar.html'
          }
        }

      })

  .state('profile', {
    url: '/profile',
    views: {
      "": {
        templateUrl: 'modules/profile/view/profile.html',
        controller: 'profileController',
        resolve: {
          auth: authenticate
        }
      },
      "header": {
        templateUrl: 'includes/header.html'
      },
      "sidebar": {
        templateUrl: 'includes/sidebar.html'
      }
    }

  })
    .state('seller_settings', {
      url: '/settings',
      views: {
        "": {
          templateUrl: 'modules/mws/view/settings.html',
          controller: 'MWSController',
          resolve: {
            auth: authenticate
          }
        },
        "header": {
          templateUrl: 'includes/header.html'
        },
        "sidebar": {
          templateUrl: 'includes/sidebar.html'
        }
      }

    })
  //
});


app.directive('dropMenus', function() {
  return {


    link: function(scope, element) {

      element.bind('click', function(e) {
        scope.findParent = element.parent();
        var parentVal = angular.element(scope.findParent);

        console.log("PAREMTJHCVJHFJHV ", parentVal.parent().prevUntil().prevObject.prevObject[0].className);
        scope.sibiling = element.next();
        var targetval = angular.element(scope.sibiling);
        targetval.slideToggle();
        parentVal.toggleClass('dropdownopen');

      });
    }
  }

});


app.directive('ngConfirmClick', [
  function() {
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.bind('click', function(e) {
          var message = attrs.ngConfirmClick;
          if (message && !confirm(message)) {
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    }
  }
]);

app.directive('myEnter', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if (event.which === 13) {
        scope.$apply(function() {
          scope.$eval(attrs.myEnter);
        });
        event.preventDefault();
      }
    });
  };
});

app.directive('multipleEmails', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {

        var emails = viewValue.split(',');
        // define single email validator here
        var re = /\S+@\S+\.\S+/;

        // angular.foreach(emails, function() {
        var validityArr = emails.map(function(str) {
          return re.test(str.trim());
        }); // sample return is [true, true, true, false, false, false]

        var atLeastOneInvalid = false;
        angular.forEach(validityArr, function(value) {
          if (value === false)
            atLeastOneInvalid = true;
        });
        if (!atLeastOneInvalid) {
          // ^ all I need is to call the angular email checker here, I think.
          ctrl.$setValidity('multipleEmails', true);
          return viewValue;
        } else {
          ctrl.$setValidity('multipleEmails', false);
          return undefined;
        }
        // })
      });
    }
  };
});


app.directive('validNumber', function() {
  return {
    require: '?ngModel',
    link: function(scope, element, attrs, ngModelCtrl) {
      if (!ngModelCtrl) {
        return;
      }

      ngModelCtrl.$parsers.push(function(val) {
        if (angular.isUndefined(val)) {
          var val = '';
        }
        // console.log("val",val);
        var clean = val.replace(/[^-0-9\.]/g, '');
        // console.log("clean",clean);
        var negativeCheck = clean.split('-');
        // console.log("negativeCheck",negativeCheck);
        var decimalCheck = clean.split('.');
        // console.log("decimalCheck",decimalCheck);
        if (!angular.isUndefined(negativeCheck[1])) {
          negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
          clean = negativeCheck[0] + '-' + negativeCheck[1];
          if (negativeCheck[0].length > 0) {
            clean = negativeCheck[0];
          }
        }

        if (!angular.isUndefined(decimalCheck[1])) {
          decimalCheck[1] = decimalCheck[1].slice(0, 2);
          clean = decimalCheck[0] + '.' + decimalCheck[1];
        }

        if (val !== clean && parseInt(clean) > 0) {
          ngModelCtrl.$setViewValue(clean);
          ngModelCtrl.$render();
        }
        if (parseInt(clean) < 0) {
          ngModelCtrl.$setViewValue('');
          ngModelCtrl.$render();
        }
        // console.log("clean value",clean);
        ngModelCtrl.$setViewValue(clean);
        ngModelCtrl.$render();
        return clean;
      });

      element.bind('keypress', function(event) {
        if (event.keyCode === 32) {
          event.preventDefault();
        }
      });
    }
  };
});


app.directive('validAlpha', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attr, ngModelCtrl) {
      function fromUser(text) {
        var transformedInput = text.replace(/[^A-Za-z ]/g, '');
        if (transformedInput !== text) {
          ngModelCtrl.$setViewValue(transformedInput);
          ngModelCtrl.$render();
        }
        return transformedInput;
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  };
});


app.directive('validAlphaNumeric', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attr, ngModelCtrl) {
      function fromUser(text) {
        var transformedInput = text.replace(/[^0-9A-Za-z ]/g, '');
        if (transformedInput !== text) {
          ngModelCtrl.$setViewValue(transformedInput);
          ngModelCtrl.$render();
        }
        return transformedInput;
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  };
});



app.directive('baseSixtyFourInput', ['$window', function($window) {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attrs, ngModel) {
      var fileObject = {};

      scope.$watch(attrs.ngModel, function() {
        currentValue = ngModel.$modelValue;
        if (currentValue) {
          fileObject = currentValue;
          ngModel.$setViewValue(fileObject);
        }
      });

      scope.readerOnload = function(e) {
        var base64 = _arrayBufferToBase64(e.target.result);
        fileObject.base64 = base64;
        scope.$apply(function() {
          ngModel.$setViewValue(fileObject);
        });
      };

      var reader = new FileReader();
      reader.onload = scope.readerOnload;

      elem.on('change', function() {
        console.log("files???????????????", elem);
        /*
         currentValue = ngModel.$modelValue;
         if (currentValue){
         fileObject = currentValue;
         ngModel.$setViewValue(fileObject);
         }
         */
        var file = elem[0].files[0];
        fileObject.filetype = file.type;
        fileObject.filename = file.name;
        fileObject.filesize = file.size;
        fileObject.isNew = true;
        reader.readAsArrayBuffer(file);
      });

      //http://stackoverflow.com/questions/9267899/arraybuffer-to-base64-encoded-string
      function _arrayBufferToBase64(buffer) {
        var binary = '';
        var bytes = new Uint8Array(buffer);
        var len = bytes.byteLength;
        for (var i = 0; i < len; i++) {
          binary += String.fromCharCode(bytes[i]);
        }
        return $window.btoa(binary);
      }
    }
  };
}]);

app.directive('fileModel', ['$parse', function($parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;

      element.bind('change', function() {
        scope.$apply(function() {
          modelSetter(scope, element[0].files[0]);
        });
      });
    }
  };
}]);

app.directive('toggle', function(){
  return {
    restrict: 'A',
    link: function(scope, element, attrs){
      if (attrs.toggle=="tooltip"){
        $(element).tooltip();
      }
      if (attrs.toggle=="popover"){
        $(element).popover();
      }
    }
  };
})

app.filter('truncate', function () {
        return function (text, length, end) {
            if (isNaN(length))
                length = 10;

    if (end === undefined)
      end = "...";

    if (text.length <= length || text.length - end.length <= length) {
      return text;
    } else {
      return String(text).substring(0, length - end.length) + end;
    }

  };
});
