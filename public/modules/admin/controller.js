var app = angular.module('pricingAdmin', ['ui.router', 'ngTable', 'toastr', 'base64', 'ui.bootstrap', 'ngDialog', 'ngAnimate']);

app.config(function($stateProvider, $urlRouterProvider) {
  var authenticate = function($q, $http, $window, $location, toastr) {
    var deferred = $q.defer();

    $http.get('/admin/checkSession').then(function(response) {
      console.log("*-------", response)
      if (response.data.messageId == 200) {
        deferred.resolve("Success");
      } else {
        deferred.reject();
        var protocol = $location.protocol();
        var host = protocol + '://' + $location.host();
        var port = $location.port();
        var sublink = "admin";
        var FullLink = host + ':' + port + '/' + sublink;
        $window.location.href = FullLink;
      }
      return deferred.promise;
    })
  };

  var checkLogOut = function($q, $http, $window, $location, toastr) {
    var deferred = $q.defer();
    $http.get('/admin/checkSession').then(function(response) {

      if (response.data.messageId == 200) {
        deferred.reject();
        var protocol = $location.protocol();
        var host = protocol + '://' + $location.host();
        var port = $location.port();
        var sublink = "admin";
        var FullLink = host + ':' + port + '/' + sublink + '#admindashboard';
        $window.location.href = FullLink;
      } else {
        deferred.resolve("Success");
      }
      return deferred.promise;
    })
  }



  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('admin', {
      url: '/',
      views: {
        "": {
          templateUrl: 'modules/admin/view/login.html',
          controller: 'adminController',
          resolve: {
            auth: checkLogOut
          }
        }
      }
    })
    .state('admin_dashboard', {
      url: '/admindashboard',
      views: {
        "": {
          templateUrl: 'modules/admin/view/dashboard.html',
          controller: 'adminController',
          resolve: {
            auth: authenticate
          }
        },
        "header": {
          templateUrl: 'includes/admin/header.html'
        },
        "sidebar": {
          templateUrl: 'includes/admin/sidebar.html'
        }
      }
    })
    .state('user', {
      url: '/user',
      views: {
        "": {
          templateUrl: 'modules/admin/view/user.html',
          controller: 'adminController',
          resolve: {
            auth: authenticate
          }
        },
        "header": {
          templateUrl: 'includes/admin/header.html'
        },
        "sidebar": {
          templateUrl: 'includes/admin/sidebar.html'
        }
      }

    })
    .state('amazon_shipping', {
      url: '/amazon_shipping',
      views: {
        "": {
          templateUrl: 'modules/admin/view/amazon_shipping.html',
          controller: 'shippingController',
          resolve: {
            auth: authenticate
          }
        },
        "header": {
          templateUrl: 'includes/admin/header.html'
        },
        "sidebar": {
          templateUrl: 'includes/admin/sidebar.html'
        }
      }

    })
    .state('amazon_referral_fee', {
      url: '/amazon_referral_fee',
      views: {
        "": {
          templateUrl: 'modules/admin/view/amazon_referral_fee.html',
          controller: 'shippingController',
          resolve: {
            auth: authenticate
          }
        },
        "header": {
          templateUrl: 'includes/admin/header.html'
        },
        "sidebar": {
          templateUrl: 'includes/admin/sidebar.html'
        }

      }
    })
    .state('forgotPassword', {
      url: '/forgotPassword',
      templateUrl: 'modules/authentication/view/forgotPassword.html',
      controller: 'adminController',
      resolve: {
        auth: checkLogOut
      }

    })
    .state('resetPassword', {
      url: '/resetPassword/:email/:randomstring',
      templateUrl: 'modules/authentication/view/resetPassword.html',
      controller: 'adminController'
    })
    .state('subscription_plan', {
      url: '/subscription_plan',
      views: {
        "": {
          templateUrl: 'modules/admin/view/subscription.html',
          controller: 'subscriptionController',
          resolve: {
            auth: authenticate
          }
        },
        "header": {
          templateUrl: 'includes/admin/header.html'
        },
        "sidebar": {
          templateUrl: 'includes/admin/sidebar.html'
        }
      }

    })
    .state('ebayFees', {
      url: '/ebayFees',
      views: {
        "": {
          templateUrl: 'modules/admin/view/ebayFees.html',
          controller: 'shippingController',
          resolve: {
            auth: authenticate
          }
        },
        "header": {
          templateUrl: 'includes/admin/header.html'
        },
        "sidebar": {
          templateUrl: 'includes/admin/sidebar.html'
        }
      }

    })
    .state('ebayUpgradeFees', {
      url: '/ebayUpgradeFees',
      views: {
        "": {
          templateUrl: 'modules/admin/view/ebayUpgradeFees.html',
          controller: 'shippingController',
          resolve: {
            auth: authenticate
          }
        },
        "header": {
          templateUrl: 'includes/admin/header.html'
        },
        "sidebar": {
          templateUrl: 'includes/admin/sidebar.html'
        }
      }
    })

  //By Dixit
  .state('profile', {
    url: '/profile',
    views: {
      "": {
        templateUrl: 'modules/admin/view/profile.html',
        controller: 'adminController',
        resolve: {
          auth: authenticate
        }
      },
      "header": {
        templateUrl: 'includes/admin/header.html'
      },
      "sidebar": {
        templateUrl: 'includes/admin/sidebar.html'
      }
    }

  })

  .state('AddAdmin', {
    url: '/AddAdmin',
    views: {
      "": {
        templateUrl: 'modules/admin/view/addNewAdmin.html',
        controller: 'adminController',
        resolve: {
          auth: authenticate
        }
      },
      "header": {
        templateUrl: 'includes/admin/header.html'
      },
      "sidebar": {
        templateUrl: 'includes/admin/sidebar.html'
      }
    }

  })

  .state('adminList', {
    url: '/adminList',
    views: {
      "": {
        templateUrl: 'modules/admin/view/adminList.html',
        controller: 'adminController',
        resolve: {
          auth: authenticate
        }
      },
      "header": {
        templateUrl: 'includes/admin/header.html'
      },
      "sidebar": {
        templateUrl: 'includes/admin/sidebar.html'
      }
    }

  })

});

app.directive('myEnter', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if (event.which === 13) {
        scope.$apply(function() {
          scope.$eval(attrs.myEnter);
        });
        event.preventDefault();
      }
    });
  };
});

app.directive('dropMenus', function() {
  return {
    link: function(scope, element) {
      element.bind('click', function(e) {
        scope.findParent = element.parent();
        var parentVal = angular.element(scope.findParent);
        // console.log(parentVal);
        scope.sibiling = element.next();
        var targetval = angular.element(scope.sibiling);
        targetval.slideToggle();
        parentVal.toggleClass('dropdownopen');
      });
    }
  }
});

app.directive('validNumber', function() {
  return {
    require: '?ngModel',
    link: function(scope, element, attrs, ngModelCtrl) {
      if (!ngModelCtrl) {
        return;
      }

      ngModelCtrl.$parsers.push(function(val) {
        if (angular.isUndefined(val)) {
          var val = '';
        }
        // console.log("val",val);
        var clean = val.replace(/[^-0-9\.]/g, '');
        // console.log("clean",clean);
        var negativeCheck = clean.split('-');
        // console.log("negativeCheck",negativeCheck);
        var decimalCheck = clean.split('.');
        // console.log("decimalCheck",decimalCheck);
        if (!angular.isUndefined(negativeCheck[1])) {
          negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
          clean = negativeCheck[0] + '-' + negativeCheck[1];
          if (negativeCheck[0].length > 0) {
            clean = negativeCheck[0];
          }
        }

        if (!angular.isUndefined(decimalCheck[1])) {
          decimalCheck[1] = decimalCheck[1].slice(0, 2);
          clean = decimalCheck[0] + '.' + decimalCheck[1];
        }

        if (val !== clean && parseInt(clean) > 0) {
          ngModelCtrl.$setViewValue(clean);
          ngModelCtrl.$render();
        }
        if (parseInt(clean) < 0) {
          ngModelCtrl.$setViewValue('');
          ngModelCtrl.$render();
        }
        // console.log("clean value",clean);
        ngModelCtrl.$setViewValue(clean);
        ngModelCtrl.$render();
        return clean;
      });

      element.bind('keypress', function(event) {
        if (event.keyCode === 32) {
          event.preventDefault();
        }
      });
    }
  };
});


app.directive('ngConfirmClick', [
  function() {
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.bind('click', function(e) {
          var message = attrs.ngConfirmClick;
          if (message && !confirm(message)) {
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    }
  }
]);



app.controller('adminController', function($scope, $http, adminService, rememberMe, $base64, $location, $window, ngTableParams, toastr, $stateParams, $rootScope, $filter) {
  $scope.obj = {
    loading: false
  }
  $scope.admin = {};


  if (rememberMe('7ZXYZ@L') != "" && rememberMe('AUU@#90') != "") {
    if ((rememberMe('7ZXYZ@L') != null && rememberMe('AUU@#90') != null)) {
      // console.log("i got value");
      $scope.admin.username = $base64.decode(rememberMe('7ZXYZ@L'));
      $scope.admin.password = $base64.decode(rememberMe('AUU@#90'));
      $scope.remember = true;
    } else {
      // console.log("No value");
      $scope.admin.username = "";
      $scope.admin.password = "";

    }
  }



  $scope.adminLogin = function(loginDetails) {

    //This will check whether the cookie has value or not
    if ($scope.remember) {
      rememberMe('7ZXYZ@L', $base64.encode($scope.admin.username));
      rememberMe('AUU@#90', $base64.encode($scope.admin.password));
    } else {
      // console.log('In else condition-- ');
      rememberMe('7ZXYZ@L', '');
      rememberMe('AUU@#90', '');
    }

    adminService.adminLogin(loginDetails).then(function(response) {
      if (response) {
        console.log("response1234", response);
        var protocol = $location.protocol();
        var host = protocol + '://' + $location.host();
        var port = $location.port();
        var sublink = "admin";
        var FullLink = host + ':' + port + '/' + sublink + '#' + '/admindashboard';

        $window.location.href = FullLink;
      }
    }, function(error) {
      if (error) {
        console.log("error", error);
        toastr.error("Invalid login details!! Please try again");
      }

    })
  }
  $scope.getAllUsers = function(search) {

    var passingSellerData = {};
    $scope.obj.loading = true;
    if (search) {
      passingSellerData.search = $scope.obj.search;

    } else {
      $scope.obj.search = "";
    }


    $scope.tableParams = new ngTableParams({
      page: 1,
      count: 5,
      sorting: {
        createdOn: "desc"
      }
    }, {
      counts: [],
      getData: function($defer, params) {
        $scope.obj.loading = true;
        passingSellerData.page = params.page();
        passingSellerData.count = params.count();
        passingSellerData.sort = params.sorting();
        adminService.listUsers(passingSellerData).success(function(response) {
          $scope.obj.loading = false;
          $scope.obj.serial = (passingSellerData.page - 1) * passingSellerData.count

          params.total(response.count);
          $scope.data = params.sorting() ? $filter('orderBy')(response.data, params.orderBy()) : response.data;
          console.log("###", $scope.data);
          $scope.userStatus = $scope.data.status;


          $scope.count = response.count;
          $defer.resolve($scope.data);

        })
      }
    })
  }
  $scope.editMode = false;
  $scope.cancelAdminlist = function() {
    $scope.editMode = false;
    alert($scope.editMode);
    $scope.adminList();

  }



  $scope.updatesubAdminInfo = function(data) {
    $scope.obj.loading = true;
    adminService.updateSubAdminInfo(data).success(function(response) {
      if (response) {
        $scope.obj.loading = false;
        toastr.success("subAdmin successfully updated!");
      } else {
        toastr.error("Error updating subAdmin!");
      }
    })
  }

  $scope.deletesubAdmin = function(data) {
    $scope.obj.loading = true;
    console.log("data", data);
    adminService.deletesubAdmin(data).success(function(response) {
      if (response) {
        $scope.obj.loading = false;
        toastr.success("subAdmin deleted successfully!");
        $scope.adminList();
      } else {
        toastr.error("Error deleting subAdmin!");
      }

    })
  }

  $scope.exportFile = function() {
    $scope.obj.loading = true;
    adminService.exportFile().success(function(response) {
      var b = new Blob([response], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });
      saveAs(b, "inventory.xlsx"); //this is FileSaver.js function
      $scope.obj.loading = false;
    }).error(function(err) {
      console.log("err>>", err);
    })
  }


  $scope.validateUser = function(email) {
    adminService.forgotPassword(email).then(function(response) {
      if (response) {
        var protocol = $location.protocol();
        var host = protocol + '://' + $location.host();
        var port = $location.port();
        var sublink = "admin";
        var FullLink = host + ':' + port + '/' + sublink;
        $window.location.href = FullLink;
      }
    })
  }



  //Reset Password
  $scope.resetPassword = function(details) {
    details.email = $stateParams.email;
    details.randomcode = $stateParams.randomstring;
    adminService.resetAdminPassword(details).then(function(response) {
      if (response) {
        var protocol = $location.protocol();
        var host = protocol + '://' + $location.host();
        var port = $location.port();
        var sublink = "admin";
        var FullLink = host + ':' + port + '/' + sublink;
        $window.location.href = FullLink;
      }
    })
  }


  $scope.deleteUser = function(info) {
    console.log("info", info);
    var id = info._id;
    $scope.obj.loading = true;
    adminService.deleteUser({
      id: id
    }).then(function(success) {
      if (success) {
        $scope.obj.loading = false;
        toastr.success("User deleted Successfully");
        $scope.getAllUsers();
      }
    }, function(error) {
      toastr.error("Problem deleting user");
    })
  }


  $scope.checkAdmin = function() {
    $http.get('/admin/checkSession').then(function(response) {
      console.log(response.data.messageId)
      if (response.data.messageId === 200) {
        var loginAdmin = response.data.data.user;
        console.log("logi'", loginAdmin)
        $scope.designation = loginAdmin.designation;
        console.log($scope.designation);
      }
    })
  }



  $scope.changeStatus = function(value) {
    console.log("You Are Going to change Your Status....");
    adminService.changeUserStatus(value).success(function(response) {
      if (response.data.nModified == 1) {
        alert("Do you want to change your status.....");
        $scope.getAllUsers();
        toastr.success("Successfully Changed Status");
      } else {
        $scope.getAllUsers();
        toastr.error("Can't Updated");
      }
    })
  }



  $scope.viewDetails = function(data) {
    var userInfo = data;
    $scope.$emit('usersValue', userInfo);
    $('#viewUserDetails').modal('show');
  }

  $scope.viewsubAdminDetails = function(data) {
    var subAdminInfo = data;
    $scope.$emit('viewsubAdminDetails', subAdminInfo);
    $('#viewsubAdminDetails').modal('show');
  }


  $scope.addAdmin = function(user) {
    console.log("New User Want To Add");
    $scope.obj.loading = true;
    adminService.addNewAdmin(user).success(function(response) {
      console.log("Add Admin Response is : ", response);
      toastr.success("Admin Successfully Registered");
      $scope.obj.loading = false;
    });
  }

  $scope.adminList = function(search) {

    var passingAdminData = {};
    $scope.obj.loading = true;
    if (search) {
      passingAdminData.search = $scope.obj.search;
    } else {
      $scope.obj.search = "";
    }
    $scope.data = [];

    $scope.tableParams = new ngTableParams({
      page: 1,
      count: 10,
      sorting: {
        createdOn: "desc"
      }
    }, {
      counts: [],
      getData: function($defer, params) {
        $scope.obj.loading = true;
        passingAdminData.page = params.page();
        passingAdminData.count = params.count();
        passingAdminData.sort = params.sorting();
        adminService.getAdminlist(passingAdminData).success(function(response) {
          $scope.obj.loading = false;
          $scope.obj.serial = (passingAdminData.page - 1) * passingAdminData.count
          params.total(response.count);
          $scope.data = response.data;
          console.log('here', $scope.data);
          $scope.userStatus = $scope.data.status;

          $defer.resolve($scope.data);

        })
      }
    })
  }


  $scope.userDetails = function() {
    $scope.obj.loading = true;
    adminService.getUserDetails().success(function(response) {
      $scope.obj.loading = false;
      $scope.profile = response.data;
    })
  }

  $scope.updateAdminUserInfo = function(data) {
    $scope.obj.loading = true;
    adminService.updateAdminUser(data).success(function(response) {
      if (response) {
        $scope.obj.loading = false;
        toastr.success("Admin Successfully Updated");
      } else {
        toastr.error("Problem updating admin Info");
      }

    })
  }



});


//Remember Me (Username and Password on login page)
app.factory('rememberMe', function() {
  function fetchValue(name) {
    var gCookieVal = document.cookie.split("; ");
    for (var i = 0; i < gCookieVal.length; i++) {

      var gCrumb = gCookieVal[i].split("=");

      if (name === gCrumb[0]) {

        var value = '';
        try {

          //value = angular.fromJson(gCrumb[1]);
          value = gCookieVal[i].substring(8).replace(/\s+/g, '');

        } catch (e) {
          value = unescape(gCrumb[1]);
        }
        return value;
      }
    }
    // a cookie with the requested name does not exist
    return null;
  }
  return function(name, values) {

    if (arguments.length === 1) return fetchValue(name);
    var cookie = name + '=';

    if (typeof values === 'object') {
      var expires = '';
      cookie += (typeof values.value === 'object') ? angular.toJson(values.value) + ';' : values.value + ';';
      if (values.expires) {
        var date = new Date();
        date.setTime(date.getTime() + (values.expires * 24 * 60 * 60 * 1000));
        expires = date.toGMTString();
      }
      cookie += (!values.session) ? 'expires=' + expires + ';' : '';
      cookie += (values.path) ? 'path=' + values.path + ';' : '';
      cookie += (values.secure) ? 'secure;' : '';
    } else {
      var d = new Date();
      d.setTime(d.getTime() + (7 * 24 * 60 * 60 * 1000));
      var expires = "expires=" + d.toUTCString()
      cookie += values + ';' + expires;
    }

    document.cookie = cookie;
    console.log("cookie1234", cookie);

  }
})

//END


//Amazon Shipping Credit Controller
app.controller('shippingController', function($scope, adminService, toastr, ngTableParams, $filter) {
  $scope.obj = {
    loading: false
  }
  $scope.is_editing = false;
  //Amazon
  //Function to show amazon shipping credits
  $scope.getAmazonShippingCredits = function(search) {
    var amazonPassingData = {};
    $scope.obj.loading = true;
    if (search) {
      amazonPassingData.search = $scope.obj.search;
    } else {
      $scope.obj.search = "";
    }
    adminService.getAmazonShippingCredits(amazonPassingData).then(function(result) {
      if (result) {
        $scope.amazonData = result.data.data;
        $scope.obj.loading = false;
      } else {

      }
    })
  }

  $scope.editRow = function(data) {

    $scope.editing = $scope.amazonData.indexOf(data);

  }

  $scope.updateData = function(data) {
    console.log("data123", data);
    $scope.obj.loading = true;
    adminService.updateAmazonShippingCredits(data).then(function(response) {
      if (response) {
        $scope.obj.loading = false;
        toastr.success("Updated Successfully.");
      }
    }, function(error) {

      toastr.error("Please try again.");
    })
  }

  $scope.getAmazonReferralFee = function(search) {
    var passingData = {};
    if (search) {
      passingData.search = $scope.obj.search;
    } else {
      $scope.obj.search = "";
    }
    $scope.tableParams = new ngTableParams({
      page: 1,
      count: 10,
      sorting: {
        name: 'asc'
      }
    }, {
      counts: [],

      getData: function($defer, params) {
        $scope.obj.loading = true;
        passingData.page = params.page();
        passingData.count = params.count();
        passingData.sort = params.sorting();

        adminService.getAmazonReferralFee(passingData).success(function(response) {

          $scope.obj.loading = false;
          $scope.obj.serial = (passingData.page - 1) * passingData.count

          params.total(response.count);
          $scope.data = params.sorting() ? $filter('orderBy')(response.data, params.orderBy()) : response.data;

          //$scope.data = response.data;
          $scope.count = response.count;
          $defer.resolve($scope.data);
          //$scope.amazonData = response.data;
        })
      }
    })
  }

  $scope.cancelAmazonReferral = function() {
    $scope.getAmazonReferralFee();
  }

  $scope.cancelAmazonShipping = function() {
    $scope.getAmazonShippingCredits();
  }


  $scope.evalType = function(data) {

    var result = typeof(data);
    if (result == 'object') {
      return true;
    } else {
      return false;
    }

  }

  $scope.updateReferralFee = function(data) {
    console.log("data", data);
    $scope.obj.loading = true;
    adminService.updateAmazonReferralFee(data).then(function(response) {

      if (response) {
        $scope.obj.loading = false;
        toastr.success("Referral fee updated successfully.");
      }
    }, function(error) {

      toastr.error("Please try again.");
    })
  }

  //END



  //ebaY

  //Function to get ebayFee Data
  $scope.getebayFees = function() {
    $scope.obj.loading = true;
    adminService.getebayFees().then(function(response) {
      if (response) {
        console.log("response", response);
        $scope.obj.loading = false;
        $scope.ebayFeesData = response.data.data;

      }
    }, function(error) {

    })
  }

  $scope.cancelebayFees = function() {
    $scope.getebayFees();
  }

  //Function to update ebay Fees
  $scope.updateEbayFee = function(data) {
    $scope.obj.loading = true;
    adminService.updateEbayFee(data).then(function(response) {

      if (response) {
        $scope.obj.loading = false;
        toastr.success("Fee Updated Successfully.");
      }
    }, function(error) {
      toastr.error("Problem updating data");
    })
  }

  //Function to get ebayUpgrade Fees

  $scope.getebayUpgradeFees = function() {
    $scope.obj.loading = true;
    adminService.ebayUpgradeFees().then(function(response) {
      if (response) {
        // $scope.ebayUpgradeFeesData=[];
        // toastr.success("ebay Upgrade fees retrieved successfully");
        $scope.ebayUpgradeFeesData = response.data.data;
        //alert("-------",$scope.ebayUpgradeFeesData.length)
        console.log("ebayUpgradeFees", $scope.ebayUpgradeFeesData);
        $scope.obj.loading = false;

      }
    }, function(error) {
      toastr.error("Problem retrieving ebayUpgrade fees");
    })
  }
  $scope.getebayUpgradeFees();
  $scope.cancelUpgradeFees = function() {

    $scope.getebayUpgradeFees();

  }

  //Function to update ebayUpgrade Fees
  $scope.updateEbayUpgradeFee = function(data) {
      $scope.obj.loading = true;
      console.log("data here-----", data);
      adminService.updateEbayUpgradeFee(data).then(function(response) {
        if (response) {
          $scope.obj.loading = false;
          toastr.success("Fee updated successfully");
        }
      }, function(error) {
        toastr.error("Problem updating data");
      })
    }
    //END
  $scope.update = function(data) {
    console.log("----------------", data)
  }


})


app.controller('subscriptionController', function($scope, toastr, adminService) {
  $scope.obj = {
    loading: false
  }
  $scope.getSubscriptionDetails = function() {
    adminService.listSubscriptions({
      flag: true
    }).then(function(data) {
      if (data) {
        $scope.subscriptionData = data.data.data;
        console.log("$scope.subscriptionData", $scope.subscriptionData);
        toastr.success("Subscriptions retrieved successfully");
      }
    }, function(err) {
      toastr.error("Problem retrieving data");
    })

  }

  $scope.editPlan = function(data) {
    console.log("data>>", data);
    adminService.updatePlan({
      data: data
    }).then(function(response) {
      if (response) {
        toastr.success("Plan updated Successfully!!")
      }
    }, function(err) {
      toastr.error("Problem editing plan!");
    })
  }

  $scope.cancelSubscription = function() {
    $scope.getSubscriptionDetails();
  }
})