app.service('adminService', function($http, $log, $location) {
    var service = {}
    var protocol = $location.protocol();
    var host = protocol + '://' + $location.host();
    var port = $location.port();
    var sublink = "admin";
    var FullLink = host + ':' + port + '/' + sublink;


    service.adminLogin = function(data) {

        return $http.post(FullLink + '/', data)
    }

    service.listUsers = function(data) {
        return $http.post(FullLink + '/list', data);
    }

    service.listSubscriptions = function(data) {
        return $http.post(FullLink + '/listSubscriptions', data);
    }

    service.getAmazonShippingCredits = function() {
        return $http.get(FullLink + '/amazon_shipping');
    }

    service.updateAmazonShippingCredits = function(data) {
        return $http.post(FullLink + '/updateAmazonCredit', data);
    }

    service.getAmazonReferralFee = function(data) {
        console.log("354657", data)
        return $http.post(FullLink + '/amazon_referral_fee', data);
    }

    service.updateAmazonReferralFee = function(data) {
        return $http.post(FullLink + '/updateAmazonReferralFee', data);
    }

    service.forgotPassword = function(data) {
        return $http.post(FullLink + '/adminForgotPass', data);
    }

    service.resetAdminPassword = function(data) {
        return $http.post(FullLink + '/adminResetPass', data);
    }

    service.logout = function() {
        return $http.post(FullLink + '/logout')
    }

    service.exportFile = function() {
        return $http({
            url: FullLink + '/exportFile',
            method: "GET",
            headers: {
                'Content-type': 'application/json'
            },
            responseType: 'arraybuffer'
        })
    }
    service.updatePlan = function(data) {
        return $http.post(FullLink + '/update_subscription_Plan', data);
    }

    service.deleteUser = function(data) {
        return $http.post(FullLink + '/delete', data)
    }


    //eBaY
    service.getebayFees = function() {
        return $http.post(FullLink + '/ebayFees');
    }

    service.updateEbayFee = function(data) {
        return $http.post(FullLink + '/updateEbayFee', data);
    }

    service.ebayUpgradeFees = function() {
        return $http.post(FullLink + '/ebayUpgradeFees');
    }

    service.updateEbayUpgradeFee = function(data) {
            return $http.post(FullLink + '/updateEbayUpgradeFee', data);
        }
        //END


    //By Dixit
    service.addNewAdmin = function(data) {
        return $http.post(FullLink + '/addNewAdmin', data);
    }

    service.changeUserStatus = function(data) {
        return $http.post(FullLink + '/changeUserStatus', data);
    }

    service.getUserDetails = function() {
        return $http.get(FullLink + '/userDetails');
    }

    service.updateAdminUser = function(data) {
        return $http.post(FullLink + '/updateAdmin', data);
    }
    service.getAdminInfo = function(data) {
        return $http.get(FullLink + '/adminInformation');
    }
    service.getAdminlist = function(data) {
        return $http.post(FullLink + '/adminList', data)
    }

    service.updateSubAdminInfo = function(data) {
        console.log("dataaaaaaaaaaaa", data);
        return $http.post(FullLink + '/updateSubAdminInfo', data)
    }

    service.deletesubAdmin = function(data) {
        return $http.post(FullLink + '/deletesubAdmin', data)
    }

    return service;

})