app.service('profileService', function($http, $log, $location) {
	var service = {}
	var protocol = $location.protocol();
	var host = protocol + '://' + $location.host();
	var port = $location.port();
	var sublink = "profile";
	var FullLink = host + ':' + port + '/' + sublink;

    service.updateUserInfo = function(data){
    	return $http.post(FullLink + '/userInfo',data)
    }
  return service;
})
