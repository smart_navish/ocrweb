app.controller('profileController', function($scope, $location, toastr, $http, profileService) {
	$scope.obj = {};
	$scope.obj.loading= false;
	$scope.profile = {};
	$scope.userInfo = function() {
		$http.get('/login/checkSession').then(function(response) {
			console.log("response1234", response);
			if (response.data) {
				var userInfo = response.data.data.user;
			}

			if (response.data.status == "success") {
        $scope.profile._id = userInfo._id;
				$scope.profile.username = userInfo.username;
				$scope.profile.firstname = userInfo.first_name;
				$scope.profile.email = userInfo.email;
				$scope.profile.lastname = userInfo.last_name;

			}
		})
	}


	$scope.updateUserInfo = function(details) {
		console.log("details",details);
		$scope.obj.loading= true;
		profileService.updateUserInfo(details).success(function(response) {
			if (response.messageId == 200) {
				$scope.obj.loading= false;
				console.log("response", response);
				toastr.success(response.message, "Success")
			}
			else {
				$scope.obj.loading= false;
				toastr.error(response.message, "Error")
			}
		});
	}
})
