app.service('authenticationService', function($http, $log, $location) {
	var service = {}
	var protocol = $location.protocol();
	var host = protocol + '://' + $location.host();
	var port = $location.port();
	var sublink = "login";
	var FullLink = host + ':' + port + '/' + sublink;


	service.sellerLogin = function(data) {
		return $http.post(FullLink + '/login', data)
	}

	service.logout = function(){
    
		return $http.post(FullLink + '/logout')
	}

	service.forgotPassword = function(data){
		
		return $http.post(FullLink + '/forgotPassword',data)
	}

	service.resetPassword = function(data){
		
		return $http.post(FullLink+ '/resetPassword',data)
	}

	// service.getAccessToken = function(data){
	// 	console.log("dataaaaaaaaaaa",data);
	// 	return $http.post(FullLink+'/getAccessToken',data)
	// }


	return service;

})