//Routes defined for login module
var app = angular.module('pricingLogin', ['ui.router', 'base64', 'toastr']);

app.config(function($stateProvider, $urlRouterProvider) {

var loginCheck = function($q, $http, $window,$location,toastr,$state){
	// console.log("m hereer in login check")
	var deferred = $q.defer();
    // console.log("deferred", deferred);
    $http.get('/login/checkSession').then(function(response) {
    	// console.log("final response",response)
      if (response.data.messageId == 200) {
         deferred.resolve("Success");
        var protocol = $location.protocol();
        var host = protocol + '://' + $location.host();
        var port = $location.port();
        var sublink = "login";
        // var FullLink = host + ':' + port + '/' + sublink;
         var FullLink = host + ':' + port + '/dashboard';
        $window.location.href = FullLink;

      } else {
        deferred.reject();
        $state.go("login")


      }
      return deferred.promise;
    })
}



	$urlRouterProvider.otherwise('/login');
	$stateProvider
		.state('login', {
			url: '/login',
			templateUrl: 'modules/authentication/view/login.html',
			controller: 'loginController',
			resolve:{
				loginCheck
			}
		})
		.state('resetPassword', {
			url: '/resetPassword/:email/:randomstring',
			templateUrl: 'modules/authentication/view/resetPassword.html',
			controller: 'loginController'
		})
		.state('forgotPassword', {
			url: '/forgotPassword',
			templateUrl: 'modules/authentication/view/forgotPassword.html',
			controller: 'loginController'
		})
})

//END


//LOGIN CONTROLLER
app.controller('loginController', function($scope, authenticationService, $window, rememberMe, $base64, $location, $stateParams, toastr, $http) {
// console.log("check");
	if (check) {
		var check = location.search;
		var array = check.split('&');
		var value = array[1];
		// console.log("value", value);
		var codeArray = value.split('=');
		var code = codeArray[1];
		// console.log("code", code);
	}

	// if (code) {
	// 	$http.get('/login/getAccessToken').then(function(response) {

	// 		if (response) {
	// 			console.log("response", response);
	// 		} else {
	// 			console.log("error");
	// 		}

	// 	})

	// }


	$scope.login = {};
	var email = $stateParams.email;
	var randomcode = $stateParams.randomstring;
	$scope.remember = false;

	//Decode the cookie and set the fields
	// console.log("At first", rememberMe('7ZXYZ@L'));
	// console.log("At first1", rememberMe('AUU@#90'));
	// console.log("cookie123456",document.cookie);
	if (rememberMe('7ZXYZ@L') != "" && rememberMe('AUU@#90') != "") {
		if ((rememberMe('7ZXYZ@L') != null && rememberMe('AUU@#90') != null)) {
			// console.log("i got value");
			$scope.login.username = $base64.decode(rememberMe('7ZXYZ@L'));
			$scope.login.password = $base64.decode(rememberMe('AUU@#90'));
			$scope.remember = true;
		} else {
			// console.log("No value");
			$scope.login.username = "";
			$scope.login.password = "";

		}
	}

	//Login Functionality
	$scope.submitLogin = function(loginDetails) {


		//This will check whether the cookie has value or not
		if ($scope.remember) {
			rememberMe('7ZXYZ@L', $base64.encode($scope.login.username));
			rememberMe('AUU@#90', $base64.encode($scope.login.password));
		} else {
			// console.log('In else condition-- ');
			rememberMe('7ZXYZ@L', '');
			rememberMe('AUU@#90', '');
		}

		//Login
		authenticationService.sellerLogin(loginDetails).then(function(response) {
			// console.log("response", response);

			var protocol = $location.protocol();
			var host = protocol + '://' + $location.host();
			var port = $location.port();
			var FullLink = host + ':' + port + '/dashboard';
			 console.log("FullLink", FullLink);
			$window.location.href = FullLink;

		}, function(err) {
			console.log("err", err);
			toastr.error("Invalid login details!! Please try again");
		})
	}



	//Forgot Password
	$scope.validateUser = function(email) {
		authenticationService.forgotPassword(email).then(function(response) {
			if (response) {
				// var protocol = $location.protocol();
				// var host = protocol + '://' + $location.host();
				// var port = $location.port();
				// var sublink = "login";
				// var FullLink = host + ':' + port + '/';
				// $window.location.href = FullLink;
			}
		})
	}

	//Reset Password
	$scope.resetPassword = function(details) {

		details.email = $stateParams.email;
		details.randomcode = $stateParams.randomstring;
		authenticationService.resetPassword(details).then(function(response) {
			if (response) {
				var protocol = $location.protocol();
				var host = protocol + '://' + $location.host();
				var port = $location.port();
				var sublink = "login";
				var FullLink = host + ':' + port + '/';
				$window.location.href = FullLink;
			}
		})
	}

});


//Remember Me (Username and Password on login page)
app.factory('rememberMe', function() {
	function fetchValue(name) {
		console.log("name",name);
		var gCookieVal = document.cookie.split("; ");
		for (var i = 0; i < gCookieVal.length; i++) {

			var gCrumb = gCookieVal[i].split("=");

			if (name === gCrumb[0]) {

				var value = '';
				try {

					//value = angular.fromJson(gCrumb[1]);
					value = gCookieVal[i].substring(8).replace(/\s+/g, '');

				} catch (e) {
					value = unescape(gCrumb[1]);
				}
				return value;
			}
		}
		// a cookie with the requested name does not exist
		return null;
	}
	return function(name, values) {

		if (arguments.length === 1) return fetchValue(name);
		var cookie = name + '=';

		if (typeof values === 'object') {
			var expires = '';
			cookie += (typeof values.value === 'object') ? angular.toJson(values.value) + ';' : values.value + ';';
			if (values.expires) {
				var date = new Date();
				date.setTime(date.getTime() + (values.expires * 24 * 60 * 60 * 1000));
				expires = date.toGMTString();
			}
			cookie += (!values.session) ? 'expires=' + expires + ';' : '';
			cookie += (values.path) ? 'path=' + values.path + ';' : '';
			cookie += (values.secure) ? 'secure;' : '';
		} else {
			var d = new Date();
			d.setTime(d.getTime() + (7 * 24 * 60 * 60 * 1000));
			var expires = "expires=" + d.toUTCString()
			cookie += values + ';' + expires;
		}

		document.cookie = cookie;


	}

	//END

});
