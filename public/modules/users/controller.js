app.controller('usersCtrl', function($scope, $filter, ngTableParams, usersService, toastr, ngDialog, $upload, $location) {
    var protocol = $location.protocol();
    var host = protocol + '://' + $location.host();
    var port = $location.port();
    var sublink = "users";
    var Link = host + ':' + port + '/';
    var FullLink = host + ':' + port + '/' + sublink;
    $scope.$parent.leftside_close = true;
    // $scope.$parent.leftside_close = false;
    $scope.Link = Link;
    $scope.FullLink = FullLink;
    $scope.obj = {
        serial: 1,
        addSupplier: false,
        errMessage: '',
        inventorysearch: '',
        loading: false
    }
    $scope.copyData = [];
    $scope.supplier = {}
    $scope.getUsers = function(search) {
        var passingData = {};

        if (search) {
            passingData.search = $scope.obj.inventorysearch;
        } else {
            $scope.obj.inventorysearch = "";
        }
        $scope.tableParams = new ngTableParams({
            page: 1,
            count: 10,
            sorting: {
                createdOn: "desc"
            }
        }, {
            counts: [],
            getData: function($defer, params) {
                $scope.obj.loading = true;
                passingData.page = params.page();
                passingData.count = params.count();
                passingData.sort = params.sorting();
                usersService.getUsers(passingData).success(function(response) {
                    $scope.obj.loading = false;
                    $scope.obj.serial = (passingData.page - 1) * passingData.count
                    params.total(response.count);
                    $scope.data = params.sorting() ? $filter('orderBy')(response.data, params.orderBy()) : response.data;
                    $defer.resolve($scope.data);
                })
            }
        })
    }


    $scope.getSubscriptions = function(search) {
        var passingData = {};
        if (search) {
            passingData.search = $scope.obj.inventorysearch;
        } else {
            $scope.obj.inventorysearch = "";
        }
        $scope.tableParams = new ngTableParams({
            page: 1,
            count: 20,
            sorting: {
                listed_date: "desc"
            }
        }, {
            counts: [],
            getData: function($defer, params) {
                $scope.obj.loading = true;
                passingData.page = params.page();
                passingData.count = params.count();
                passingData.sort = params.sorting();
                usersService.getSubscriptions(passingData).success(function(response) {
                    $scope.obj.loading = false;
                    $scope.obj.serial = (passingData.page - 1) * passingData.count
                    params.total(response.count);
                    $scope.data = params.sorting() ? $filter('orderBy')(response.data, params.orderBy()) : response.data;
                    $defer.resolve($scope.data);
                })
            }
        })
    }

    $scope.cancel = function(row, rowForm, that, index) {
        $scope.data[index].isEditing = false;

    }

    $scope.editRow = function(row) {
        console.log("print row???",row);
        $scope.copyData.push(angular.copy(row));
    }

    $scope.del = function(row, index) {
      swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#18A1B9',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
          }).then(function () {
            usersService.updateUser(row).success(function(response) {
                $scope.getUsers();
                swal(
                  'Deleted!',
                  'User deleted successfully.',
                  'success'
                )
                toastr.success('User Sucessfully Deleted');
                $scope.obj.loading = false;
            }).error(function(error) {
                toastr.error(error.message);
                $scope.obj.errMessage = error.message
            })
            //$scope.obj.loading = true;
          })
    }

    $scope.changeStatus = function(row, val) {
      swal({
          title: 'Are you sure?',
          text: "You can chage this in future!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#18A1B9',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, change it!'
          }).then(function () {
            row.newstatusval = val;
            usersService.updateUser(row).success(function(response) {
                $scope.getUsers();
                toastr.success('User status changed sucessfully.');
                $scope.obj.loading = false;
            }).error(function(error) {
                toastr.error(error.message);
                $scope.obj.errMessage = error.message
            })
            //$scope.obj.loading = true;
          })
    }

    function resetRow(row, rowForm, that) {
        row.isEditing = false;
        rowForm.$setPristine();
        that.tableTracker.untrack(row);
        return _.findWhere(originalData, function(r) {
            return r.id === row.id;
        });
    }

    $scope.save = function(row, rowForm) {
        $scope.obj.loading = true;
        usersService.updateSupplier(row).success(function(response) {
            row.isEditing = false;
            rowForm.$setPristine();
            toastr.success('Supplier Data Sucessfully Updated');
            $scope.obj.loading = false;
        }).error(function(error) {
            toastr.error(error.message);
            $scope.obj.errMessage = error.message
        })
    }


    $scope.addSupplier = function() {
        $scope.obj.errMessage = '';
        if ($scope.supplier && $scope.supplier.name && $scope.supplier.email && $scope.supplier.phone && $scope.supplier.city && $scope.supplier.state && $scope.supplier.country) {
            $scope.obj.loading = true;
            usersService.addSupplier($scope.supplier).success(function(response) {
                toastr.success('Suplier Added Sucessfully');
                $scope.obj.loading = false;
                $scope.data.push(response.data);
                $scope.obj.errMessage = '';
                $scope.obj.addSupplier = false;
            }).error(function(error) {
                toastr.error(error.message);
                $scope.obj.errMessage = error.message
            })
        } else {
            toastr.error('Plese Fill Details');
        }
    }

    $scope.uploadCatalogPopup = function() {
        ngDialog.open({
            template: '/modules/supplier/view/progress.html',
            className: 'ngdialog-theme-plain',
            scope: $scope
        });
    }

    $scope.onFileSelectVariationZero = function($files, index) {

        if($files.length){
        if ($files[0].type == "image/jpeg" || $files[0].type == "image/png"||$files[0].type=="application/pdf") {
            for (var i = 0; i < $files.length; i++) {
                $scope.uploadCatalogPopup();
                $scope.obj.loading = true;
                var file = $files[i];
                $scope.upload = $upload.upload({
                    url: $scope.FullLink + '/upload',
                    method: 'POST',
                    withCredentials: true,
                    data: {
                        type: 'variant'
                    },
                    file: file,
                }).progress(function(evt) {
                    console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                    $scope.obj.progress = parseInt(100.0 * evt.loaded / evt.total);
                }).success(function(data, status, headers, config) {
                    ngDialog.closeAll();
                    var supplier = $scope.data[index];
                    supplier["catalog"] = data;
                    usersService.updateSupplier(supplier).success(function(response) {
                        toastr.success('Supplier Catalog Sucessfully Added');
                        $scope.data[index].catalog = data;
                        $scope.obj.loading = false;
                    }).error(function(error) {
                        toastr.error(error.message);
                        $scope.obj.errMessage = error.message
                    })
                    $scope.obj.loading = false;
                }).error(function(err) {

                })
            }
        }else{
            toastr.error("Only images and pdfs are allowed!");
        }

    }


    }

});
