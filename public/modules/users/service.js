app.service('usersService', function($http, $log, $location) {
    var service = {}
    var protocol = $location.protocol();
    var host = protocol + '://' + $location.host();
    var port = $location.port();
    var sublink = "users";
    var FullLink = host + ':' + port + '/' + sublink;
    service.getUsers = function(data) {
        return $http.post(FullLink + '/getUsers', data)
    }

    service.getSubscriptions = function(data) {
        return $http.post(FullLink + '/getSubscriptions', data)
    }

    service.addSupplier = function(data) {
        return $http.post(FullLink + '/addSupplier', data)
    }

    service.updateUser = function(data) {
        return $http.put(FullLink + '/updateUser', data)
    }
    return service;
})
