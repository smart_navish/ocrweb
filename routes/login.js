exports.login = function(app, express, passport) {
	var loginController = require('./../app/controller/loginController');
	var router = express.Router();
	
	router.post('/login', passport.authenticate('local', {
		session: true
	}), loginController.login);

	router.get('/checkSession',loginController.checkSession);
	router.post('/logout',loginController.logOut);
    router.post('/forgotPassword',loginController.forgotPassword);
    router.post('/resetPassword',loginController.resetPassword);
    // router.post('/getAccessToken',function(req,res){
    // 	console.log("req................",req.body);
    // 	// var newUrl = 'https://signin.sandbox.ebay.com/ws/eBayISAPI.dll?oAuthRequestAccessToken&client_id=Akanksha-PricingE-SBX-169dbd521-7631471d&redirect_uri=Akanksha_Sood-Akanksha-Pricin-xbbharh&client_secret=SBX-69dbd521eccf-89dc-4f1a-a2d4-266a&code='+data;
    // 	// res.redirect(newUrl,function(err,res){
    // 	// 	console.log("ressss12433663",res.access_token);
    // 	// });
    // });
    router.getAccessToken



	app.use('/login', router);
}