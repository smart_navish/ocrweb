var express = require('express');
var router = express.Router();
var path = require('path');


/* GET home page. */
/**
if you cahnge /dashboard route name then need to change in
1.  submitLogin () function FullLink in modules/authentication/controller

*/

router.get('/dashboard', function(req, res, next) {
    res.sendFile(path.join(__dirname + './../public/seller.html'));
});
router.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname + './../public/login.html'));
});

router.get('/admin', function(req, res, next) {
    res.sendFile(path.join(__dirname + './../public/admin.html'));
});

router.get('/privatepolicy', function(req, res, next) {
    res.sendFile(path.join(__dirname + './../public/privatepolicy.html'));
});

module.exports = router;
