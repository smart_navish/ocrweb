exports.users = function (app, express){
  var usersCtrl = require('./../app/controller/usersController');
  //Get warehouse
  router = express.Router();
  router.post('/getusers', usersCtrl.getusers);
  router.post('/getSubscriptions', usersCtrl.getSubscriptions);

  //Update Supplier
  router.put('/updateUser', usersCtrl.updateUser);

  router.post('/upload', usersCtrl.uploadFile);


  app.use('/users', router);
}
