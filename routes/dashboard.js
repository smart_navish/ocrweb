module.exports = function(app,express){
    var dashboardController = require('./../app/controller/dashboard');
    var router = express.Router();

    router.get('/getDashboard', dashboardController.getDashboard);
    router.get('/pendingStock',dashboardController.pendingStock);
     router.get('/lowStock',dashboardController.lowStock);
     router.get('/inventoryStock',dashboardController.inventoryStock);



  router.post("/orderDetail",dashboardController.orderDetail)

   router.get("/revenueDetail",dashboardController.revenueDetail)

    app.use('/dashboard', router);
}
