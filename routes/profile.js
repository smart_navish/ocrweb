module.exports = function(app, express){
  var profileCtrl = require('./../app/controller/profileCtrl');
  var router = express.Router();

  router.post('/userInfo', profileCtrl.updateProfile);


  app.use('/profile', router);
}
