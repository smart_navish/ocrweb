module.exports = function (app, express) {
  var router = express.Router();

    var userObj = require('./../app/controller/apisController.js');
    var authApi = require('./../app/auth.js');

    router.get('/', function(req, res) {
      res.send('respond with a resource');
    });
    router.get('/allLanguage', userObj.allLanguage);
    router.get('/allSubscriptions', userObj.allSubscriptions);
    router.post('/registerUser', userObj.registerUser);
    router.post('/confirmationEmail', userObj.confirmationEmail);
    router.post('/login', userObj.login);
    router.post('/fb_login', userObj.fb_login);
    router.post('/forgotPassword', userObj.forgotPassword);
    router.post('/forgotPasswordConfirmation', userObj.forgotPasswordConfirmation);
    router.post('/tokenConfirmation', userObj.tokenConfirmation); //template


    router.use(authApi.auth);

    router.post('/profileDetail', userObj.profileDetail);
    router.post('/changePassword', userObj.changePassword);
    router.post('/updateProfile', userObj.updateProfile);
    router.post('/resetPassword', userObj.resetPassword);
    router.post('/addSubscription', userObj.addSubscription);
    app.use('/apis', router);
}
