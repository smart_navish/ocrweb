module.exports = function(app, express, passport) {
	var adminController = require('./../app/controller/adminController');
	var router = express.Router();

	router.post('/', passport.authenticate('localadmin', {
		session: true
	}), adminController.login);

    router.get('/checkSession',adminController.checkSession);
    router.post('/list',adminController.userlist);
    router.get('/amazon_shipping',adminController.getAmazonShippingCredit);	
    router.post('/updateAmazonCredit',adminController.updateAmazonCredit);
    router.post('/amazon_referral_fee',adminController.amazonReferralFee);	
    router.post('/updateAmazonReferralFee',adminController.updateAmazonReferralFee);
    router.post('/adminForgotPass',adminController.adminForgotPassword);	
    router.post('/adminResetPass',adminController.adminResetPassword);
    router.post('/logout',adminController.logOut);
    router.get('/exportFile', adminController.exportFile);
    router.post('/listSubscriptions',adminController.listSubscriptions);
    router.post('/update_subscription_Plan',adminController.updatePlan);
    router.post('/delete',adminController.deleteUser);
    router.post('/ebayFees',adminController.getebayFees);
    router.post('/updateEbayFee',adminController.updateEbayFee);
    router.post('/ebayUpgradeFees',adminController.getebayUpgradeFees);
    router.post('/updateEbayUpgradeFee',adminController.updateEbayUpgradeFee);
 
    router.post('/addNewAdmin',adminController.addNewAdmin);
    router.post('/changeUserStatus',adminController.changeUserStatus);
    router.get('/userDetails',adminController.userDetails);
    router.post('/updateAdmin',adminController.updateAdmin);
    router.get('/adminInformation',adminController.adminInformation);
    router.post('/adminList',adminController.adminList);
    router.post('/updateSubAdminInfo',adminController.updateSubAdminInfo);
    router.post('/deletesubAdmin',adminController.deletesubAdmin);

    app.use('/admin', router);
}