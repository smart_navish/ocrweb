var config = {
  'production': {
    //  'database': 'mongodb://pricingeasy:pricingeasy2780@localhost/pricingeasy'
  },
  'staging' : {
      'database': 'mongodb://pricingeasy:pricingeasy2780@localhost/pricingeasy'
  },
  'development' : {
      'database': 'mongodb://localhost/ocrapp'
  }
}
exports.get = function (env){
  return config[env];
}
