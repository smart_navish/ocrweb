var env = require('./env');
var config = require('./config.js').get(env.environment);
console.log(env)
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var db = require('./app/config/db')(config);
var index = require('./routes/index');
var fileUpload = require('express-fileupload');
var authentication = require('./app/config/passport.js');
var passport = require('passport');
var path = require('path');
var session = require('express-session');
var app = express();


app.use(passport.initialize());
app.use(passport.session());
app.use(session({
	secret: 'keyboard-cat',
	cookie: {
		maxAge: 26280000000000
	},
	proxy: true,
	resave: true,
	saveUninitialized: true
}));
//END

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(fileUpload());
app.use(logger('dev'));
app.use(bodyParser.json({limit: '20mb'}));
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
require('./app/config/passport.js')(app, express, passport);
require('./routes/users').users(app, express);
require('./routes/login').login(app, express,passport);
require('./routes/profile')(app,express);
require('./routes/dashboard')(app,express);
require('./routes/apis')(app, express);
var hourMs = 1000 * 60 * 60;
app.use(express.static(__dirname + '/uploads', {
	maxAge: hourMs
}));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found and ', req + ' end');
	err.status = 404;
	next(err);
});




// error handler
// app.use(function(err, req, res, next) {
// 	// set locals, only providing error in development
// 	res.locals.message = err.message;
// 	res.locals.error = req.app.get('env') === 'development' ? err : {};

// 	// render the error page
// 	res.status(err.status || 500);
// 	res.render('error');
// });

module.exports = app;
